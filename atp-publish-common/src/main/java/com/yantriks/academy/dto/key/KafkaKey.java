package com.yantriks.academy.dto.key;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class KafkaKey {
  private boolean dlqPublish;

  private Throwable throwable;

  private boolean acknownledgable;

}
