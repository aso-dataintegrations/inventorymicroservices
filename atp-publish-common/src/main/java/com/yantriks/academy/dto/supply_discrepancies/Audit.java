package com.yantriks.academy.dto.supply_discrepancies;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Audit {
    private String transactionId;
    private String transactionReason;
    private String transactionSystem;
    private String transactionType;
    private String transactionUser;
}
