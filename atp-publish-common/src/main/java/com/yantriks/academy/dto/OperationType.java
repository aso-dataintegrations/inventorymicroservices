package com.yantriks.academy.dto;

public enum OperationType {
  CREATE,
  UPSERT,
  DELETE,
  CACHE_LOAD,
  CACHE_LOAD_TERMINAL_MARKER;

  private OperationType() {
  }
}