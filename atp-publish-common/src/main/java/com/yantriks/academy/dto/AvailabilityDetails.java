package com.yantriks.academy.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AvailabilityDetails {
    private String segment;
    private int atp;
    private int supply;
    private int demand;
    private int safetyStock;
    private String atpStatus;
    private List<FutureQtyByDates> futureQtyByDates;
}
