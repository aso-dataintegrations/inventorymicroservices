package com.yantriks.academy.dto.supply_discrepancies;

public enum SupplyTypeEnum {
    SOH,
    TSF_RESERVED,
    RTV,
    NON_SELLABLE,
    IN_TRANSIT,
    RESERVED,
    CUSTOMER_BACKORDER,
    DISTRO,
    TSF_EXPECTED;
    private SupplyTypeEnum(){}
}
