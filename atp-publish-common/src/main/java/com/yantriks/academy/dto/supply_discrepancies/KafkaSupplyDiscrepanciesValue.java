package com.yantriks.academy.dto.supply_discrepancies;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class KafkaSupplyDiscrepanciesValue {
    @JsonProperty("Productid")
    private String productId;
    private String locationId;
    private String locationType;
    @JsonProperty("CUSTOMER_RESV")
    private Double customerResv;
    @JsonProperty("IN_TRANSIT")
    private Double inTransit;
    @JsonProperty("TSF_RESERVED")
    private Double tsfReserved;
    @JsonProperty("TSF_EXPECTED")
    private Double tsfExpected;
    @JsonProperty("ONHAND")
    private Double onhand;
    @JsonProperty("RTV")
    private Double rtv;
    @JsonProperty("NON_SELLABLE")
    private Double nonSellable;
    @JsonProperty("DISTRO")
    private Double distro;
    @JsonProperty("SOH")
    private Double soh;
    @JsonProperty("EXTRACT_TIMESTAMP")
    private String extractTimestamp;
}

