package com.yantriks.academy.dto.supply_discrepancies;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.yantriks.academy.dto.OperationType;
import com.yantriks.academy.dto.supply_discrepancies.key.KafkaSupplyDiscrepanciesKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupplyDiscrepanciesPublishEntity {
    private String topic;
    private KafkaSupplyDiscrepanciesKey key;
    private SupplyDiscrepanciesValue value;
    private OperationType operation;
    private Boolean isFullyQualifiedTopicName;
}
