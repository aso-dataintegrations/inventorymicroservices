package com.yantriks.academy.dto.supply_discrepancies.key;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class KafkaSupplyDiscrepanciesKey {
    private String orgId;
    private String productId;
    private String uom;
    private String locationId;
    private String locationType;
}
