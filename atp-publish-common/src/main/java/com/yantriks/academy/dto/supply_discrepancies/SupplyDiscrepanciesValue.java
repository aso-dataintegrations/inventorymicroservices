package com.yantriks.academy.dto.supply_discrepancies;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupplyDiscrepanciesValue {
    private String eventType;
    private String feedType;
    private String locationId;
    private String locationType;
    private String orgId;
    private String productId;
    private String uom;
    private String updateTimeStamp;
    private SupplyTo to;
    private Audit audit;
}
