package com.yantriks.academy.streamer.kafka.configuration;

import lombok.*;

import java.time.Duration;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
public class RestApiConfigurationProperties {
    private int retryLimit;
    private Duration initialBackOff;
    private Duration maxBackOff;

    public RestApiConfigurationProperties(int retryLimit, Duration initialBackOff, Duration maxBackOff){
        this.retryLimit = retryLimit;
        this.initialBackOff = initialBackOff;
        this.maxBackOff = maxBackOff;
    }
}
