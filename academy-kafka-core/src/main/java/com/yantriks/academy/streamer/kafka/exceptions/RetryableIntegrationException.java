package com.yantriks.academy.streamer.kafka.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class RetryableIntegrationException extends RuntimeException {

  private final HttpStatus httpStatus;
  private final String url;

  public RetryableIntegrationException(String message, HttpStatus httpStatus) {
    super(message);
    this.httpStatus = httpStatus;
    url = null;
  }

  public RetryableIntegrationException(String message, HttpStatus httpStatus,String url) {
    super(message);
    this.httpStatus = httpStatus;
    this.url= url;
  }
}
