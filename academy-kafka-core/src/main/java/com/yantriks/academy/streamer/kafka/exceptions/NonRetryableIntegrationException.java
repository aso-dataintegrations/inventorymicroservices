package com.yantriks.academy.streamer.kafka.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class NonRetryableIntegrationException extends RuntimeException  {
  private final HttpStatus httpStatus;

  public NonRetryableIntegrationException(String message) {
    super(message);
    this.httpStatus = null;
  }

  public NonRetryableIntegrationException(String message, HttpStatus httpStatus) {
    super(message);
    this.httpStatus = httpStatus;
  }
}
