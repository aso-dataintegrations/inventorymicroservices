package com.yantriks.academy.streamer.kafka.constants;

import lombok.*;
import org.springframework.boot.actuate.health.Status;

import java.util.Map;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HealthDto {
  private Status status;
  private Map<String, Object> components;
}
