package com.yantriks.academy.streamer.kafka.configuration;

import lombok.Builder;
import lombok.Data;

import java.time.Duration;
import lombok.Getter;
import lombok.Setter;

@Data
@Builder
@Getter
@Setter
public class KafkaConsumerConfigurationProperties {
  private boolean consumerEnabled;
  private Duration commitInterval;
  private int retryLimit;
  private int schedulerTimeToLive;
  private Duration initialBackOff;
  private Duration maxBackOff;
  private boolean dlqPublishEnabled;
  private String groupId;
  private String securityProtocol;
  private String sslMechanism;
  private String saslJaasConfg;
  private String maxPollRecords;

}
