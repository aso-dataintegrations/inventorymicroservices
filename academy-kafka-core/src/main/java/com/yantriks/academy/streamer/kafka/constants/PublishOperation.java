package com.yantriks.academy.streamer.kafka.constants;

public enum PublishOperation {
  CREATE,
  UPDATE,
  DELETE,
  UPSERT
}
