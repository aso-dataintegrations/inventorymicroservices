package com.yantriks.academy.streamer.kafka.constants;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HealthAlgolia {
  private String status;
  private String message;
  private List<Object> items;
}
