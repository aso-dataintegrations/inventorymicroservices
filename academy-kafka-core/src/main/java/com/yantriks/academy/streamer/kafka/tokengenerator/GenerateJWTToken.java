package com.yantriks.academy.streamer.kafka.tokengenerator;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.StringReader;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

@Component
@Slf4j
public class GenerateJWTToken {

    @Value("${auth.jwt.privatekey}")
    private String privateKey;

    @Value("${auth.jwt.privatekeyid}")
    private String privateKeyId;

    @Value("${auth.jwt.timetolive}")
    private String timeToLive;

    @Value("${auth.jwt.issuer}")
    private String issuer;

    @Value("${auth.jwt.audience}")
    private String audience;

    private String currentTOKEN;

    public String generateToken() throws Exception {

        RSAPrivateKey key = (RSAPrivateKey) getPrivateKey(privateKey);
        Calendar date1 = Calendar.getInstance();
        long t = date1.getTimeInMillis();
        Date afterAddingMins=new Date(t + (10 * Long. parseLong(timeToLive)));
        Date dt = new Date();

        Algorithm algorithm = Algorithm.RSA256(null,key) ;
        currentTOKEN = JWT.create()
                .withKeyId(privateKeyId)
                .withIssuer(issuer)
                .withSubject(issuer)
                .withAudience(audience)
                .withIssuedAt(dt)
                .withExpiresAt(afterAddingMins)
                .withExpiresAt(asDate(LocalDateTime.now().plusMinutes(
                        Long. parseLong(timeToLive)+1)))
                .sign(algorithm);

        log.debug("Expiry date" + asDate(LocalDateTime.now().plusMinutes(
                Long. parseLong(timeToLive)+1)));
        log.debug("Generated Token is " + currentTOKEN);
        return currentTOKEN;
    }

    private static Date asDate(LocalDateTime dateTime) {
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }


    private static PrivateKey getPrivateKey(String privateKey) throws Exception {
        // Read in the key into a String
        StringBuilder pkcs8Lines = new StringBuilder();
        BufferedReader rdr = new BufferedReader(new StringReader(privateKey));
        String line;
        while ((line = rdr.readLine()) != null) {
            pkcs8Lines.append(line);
        }
        // Remove the "BEGIN" and "END" lines, as well as any whitespace
        String pkcs8Pem = pkcs8Lines.toString();
        pkcs8Pem = pkcs8Pem.replaceAll("\\\\n", "");
        pkcs8Pem = pkcs8Pem.replace("-----BEGIN PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replace("-----END PRIVATE KEY-----", "");

        // Base64 decode the result
        byte[] pkcs8EncodedBytes = Base64.getDecoder().decode(pkcs8Pem);

        // extract the private key
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);

    }
}