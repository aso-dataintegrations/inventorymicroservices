package com.yantriks.academy.streamer.kafka.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BadRequestException   extends RuntimeException  {
    private final HttpStatus httpStatus;

    public BadRequestException(String message) {
        super(message);
        httpStatus = null;
    }

    public BadRequestException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
