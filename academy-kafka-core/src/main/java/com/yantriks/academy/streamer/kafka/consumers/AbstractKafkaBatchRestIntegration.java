package com.yantriks.academy.streamer.kafka.consumers;

import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.BACKOFF_INITIAL_PROPERTY_OVERRIDE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.BACKOFF_MAX_PROPERTY_OVERRIDE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.COMMIT_INTERVAL_PROPERTY_OVERRIDE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.CONSUMER_ENABLED_PROPERTY_OVERRIDE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.DLQ_PUBLISH_ENABLED_PROPERTY_OVERRIDE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.ERROR_CODE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.ERROR_MESSAGE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.HYPHEN;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.KAFKA_TOPIC_NAME_DELIMITERS;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.OVERRIDE_KAFKA_TOPIC_GROUP_ID_PROPERTY_PREFIX;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.OVERRIDE_KAFKA_TOPIC_NAME_PROPERTY_PREFIX;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.RETRY_LIMIT_PROPERTY_OVERRIDE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.SASL_JAAS_CONFIG;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.SASL_MECHANISM;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.SCHEDULER_TIME_TO_LIVE_PROPERTY_OVERRIDE;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.SECURITY_PROTOCOL;
import static com.yantriks.academy.streamer.kafka.constants.KafkaConstants.STATUS_CODE;
import static org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG;
import static org.springframework.kafka.support.serializer.JsonDeserializer.KEY_DEFAULT_TYPE;
import static org.springframework.kafka.support.serializer.JsonDeserializer.VALUE_DEFAULT_TYPE;

import com.algolia.search.exceptions.AlgoliaRuntimeException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yantriks.academy.dto.key.KafkaKey;
import com.yantriks.academy.streamer.kafka.configuration.CustomJsonKafkaConsumerConfiguration;
import com.yantriks.academy.streamer.kafka.configuration.CustomKafkaProducerConfiguration;
import com.yantriks.academy.streamer.kafka.configuration.KafkaConsumerConfigurationProperties;
import com.yantriks.academy.streamer.kafka.constants.HealthAlgolia;
import com.yantriks.academy.streamer.kafka.constants.HealthDto;
import com.yantriks.academy.streamer.kafka.constants.PublishOperation;
import com.yantriks.academy.streamer.kafka.exceptions.BadRequestException;
import com.yantriks.academy.streamer.kafka.exceptions.NonRetryableIntegrationException;
import com.yantriks.academy.streamer.kafka.exceptions.RetryableIntegrationException;
import io.micrometer.core.instrument.MeterRegistry;
import java.net.ConnectException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.kafka.clients.consumer.CommitFailedException;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.protocol.types.Field.Bool;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOffset;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;
import reactor.retry.RetryExhaustedException;

@Slf4j
@Component
public abstract class AbstractKafkaBatchRestIntegration<K, V> {

  @Autowired
  protected Environment env;
  @Value("${kafka.consumer.commit.interval:5s}")
  private Duration commitInterval;
  @Value("${kafka.consumer.retry.limit:0}")
  private int retryLimit;
  @Value("${ypfp.kafka.topic-name-delimiter:-}")
  private String topicNameDelimiter;
  @Value("${ypfp.consume.region}")
  private String region;
  @Value("${ypfp.customer-name}")
  private String customerName;


  @Value("${kafka.consumer.sasl.jaas.config}")
  private String jaasConfig;

  @Value("${kafka.consumer.sasl.mechanism}")
  private String saslMechanism;

  @Value("${kafka.consumer.security.protocol}")
  private String securityProtocol;

  @Autowired
  private WebClient webClient;
  private KafkaSender<K, V> dlqSender;

  @Value("${kafka.consumer.scheduler.time-to-live:60}")
  private int schedulerTimeToLive;
  @Value("${kafka.consumer.retry.backoff.initial:10s}")
  private Duration initialBackOff;
  @Value("${kafka.consumer.retry.backoff.max:1m}")
  private Duration maxBackOff;
  @Value("${kafka.consumer.dlq-publish.enabled:false}")
  private Boolean deadLetterQueuePublishEnabled;
  @Value("${kafka.consumer.circuit-open.interval-in-sec:5}")
  private int circuitOpenInterval;
  @Value("${kafka.consumer.batchConsummerSize}")
  private int batchConsummerSize;
  @Value("${kafka.consumer.batchConsumeDurationInSeconds}")
  private int batchConsumeDurationInSeconds;
  @Value("${kafka.consumer.batchValidationSize}")
  private int batchValidationSize;
  @Value("${kafka.consumer.batchValidationPoll}")
  private int batchValidationPoll;
  @Value("${kafka.consumer.fullIndex.enable:false}")
  private Boolean fullIndexEnabled;
  @Autowired
  private ObjectMapper objectMapper;

  @Value("${ypfp.kafka.dlq-suffix:DLQ}")
  private String deadLetterQueueSuffix;

  @Value("${algolia.application_key}")
  private String applicationKey;
  @Value("${algolia.admin_api_key}")
  private String adminApiKey;
  @Value("${algolia.healthStatus-url}")
  private String healthStatusUrl;

  @Autowired
  private MeterRegistry meterRegistry;

  @Autowired
  private CustomJsonKafkaConsumerConfiguration jsonConsumerPropsConfiguration;

  @Autowired
  private CustomKafkaProducerConfiguration defaultProducerPropsConfiguration;

  private Scheduler dlqSenderScheduler;
  private KafkaConsumerConfigurationProperties config;


  @PostConstruct
  public void init() {
    if (!KAFKA_TOPIC_NAME_DELIMITERS.contains(topicNameDelimiter)) {
      throw new IllegalStateException(String.format("Invalid kafka topic name delimiter '%s'. Only '-', '_', '.' and '' are acceptable values.", topicNameDelimiter));
    }

    config = generateKafkaConsumerConfigurationProperties(getTopicName());

    if (config.isDlqPublishEnabled()) {
      ThreadFactory threadFactory = new BasicThreadFactory.Builder()
          .namingPattern(getTopicName() + "-%d")
          .build();
      dlqSenderScheduler = Schedulers
          .fromExecutorService(Executors.newFixedThreadPool(4, threadFactory));
      SenderOptions<K, V> senderOptions = SenderOptions.create(defaultProducerPropsConfiguration.getProducer());
      dlqSender = KafkaSender.create(senderOptions);
    } else {
      getLogger().warn("DLQ publish disabled for topic: {}", getTopicName());
    }
  }
  /*
   Setting up Kafka Consumer configuration properties
   */
  protected KafkaConsumerConfigurationProperties generateKafkaConsumerConfigurationProperties(String topicName) {
    return KafkaConsumerConfigurationProperties.builder()
        .consumerEnabled(env.getProperty(String.format(CONSUMER_ENABLED_PROPERTY_OVERRIDE, topicName), Boolean.class, true))
        .commitInterval(env.getProperty(String.format(COMMIT_INTERVAL_PROPERTY_OVERRIDE, topicName), Duration.class, commitInterval))
        .retryLimit(env.getProperty(String.format(RETRY_LIMIT_PROPERTY_OVERRIDE, topicName), Integer.class, retryLimit))
        .schedulerTimeToLive(env.getProperty(String.format(SCHEDULER_TIME_TO_LIVE_PROPERTY_OVERRIDE, topicName), Integer.class,
            schedulerTimeToLive))
        .initialBackOff(env.getProperty(String.format(BACKOFF_INITIAL_PROPERTY_OVERRIDE, topicName), Duration.class, initialBackOff))
        .maxBackOff(env.getProperty(String.format(BACKOFF_MAX_PROPERTY_OVERRIDE, topicName), Duration.class, maxBackOff))
        .dlqPublishEnabled(env.getProperty(String.format(DLQ_PUBLISH_ENABLED_PROPERTY_OVERRIDE, topicName), Boolean.class, deadLetterQueuePublishEnabled))
        .groupId(env.getProperty(OVERRIDE_KAFKA_TOPIC_GROUP_ID_PROPERTY_PREFIX + topicName))
        .securityProtocol(env.getProperty(SECURITY_PROTOCOL))
        .sslMechanism(env.getProperty(SASL_MECHANISM))
        .saslJaasConfg(env.getProperty(SASL_JAAS_CONFIG))
        .build();
  }
  /*
   Creates the consumer for the consumerTopic and assigns
   */
  @EventListener(ApplicationReadyEvent.class)
  public void consume() {
    if (config.isConsumerEnabled()) {
      final String consumerTopicName = generateTopicName();
      Scheduler scheduler = Schedulers.newElastic("consumer-" + getTopicName(), config.getSchedulerTimeToLive(), true);
      Scheduler backoffScheduler = Schedulers.newParallel("consumer-retry-" + getTopicName(), Schedulers.DEFAULT_POOL_SIZE, true);

      Map<String, Object> consumerProps = new HashMap<>();
      consumerProps.putAll(jsonConsumerPropsConfiguration.getConsumer());

      configureConsumerProperties(consumerProps, config.getGroupId());

      ReceiverOptions<K, V> receiverOptions =
          ReceiverOptions.<K, V>create(consumerProps)
              .subscription(Collections.singleton(consumerTopicName))
              .commitInterval(config.getCommitInterval())
              .addAssignListener(p -> getLogger().info("Group partitions assigned: {}", p))
              .addRevokeListener(p -> getLogger().info("Group partitions revoked: {}", p));

      final String dlqTopicName = generateDLQTopicName();
      createKafkaReceiver(config, receiverOptions, scheduler, backoffScheduler, dlqTopicName, dlqSenderScheduler, dlqSender);
    }
  }


  private void createKafkaReceiver(KafkaConsumerConfigurationProperties config, ReceiverOptions<K, V> receiverOptions, Scheduler scheduler, Scheduler backoffScheduler,
      String dlqTopicName, Scheduler dlqSenderScheduler, KafkaSender<K, V> dlqSender) {
    KafkaReceiver.create(receiverOptions)
        .receive()
        .bufferTimeout(batchConsummerSize, Duration.ofSeconds(batchConsumeDurationInSeconds), scheduler)
        .publishOn(scheduler)
        .concatMap(receivedRecordList ->{
          if(fullIndexEnabled && receivedRecordList.size()<batchValidationSize && batchValidationPoll > 0) {
              batchValidationPoll--;
          }
          return publish(receivedRecordList, null, config.isDlqPublishEnabled(), dlqTopicName, dlqSenderScheduler, dlqSender, meterRegistry)
              .doOnComplete(() -> acknowledge(receivedRecordList));
        })
        .doOnNext(receiverRecords -> {
          if(fullIndexEnabled && batchValidationPoll<=0){
            getLogger().info("Disposing consumer {} after getting consumer buffer size less than batchValidationSize({}) for configured repoll of the batch",
                Thread.currentThread().getName(),batchValidationSize);
            scheduler.dispose();
            backoffScheduler.dispose();

          }
        })
        .onErrorContinue(this::checkIfExceptionTobeLogged,
            (t, o) -> {
              getLogger().warn("Event Dropped. Proceeding to next: {} - {} ", t.getClass(), o);
              getLogger().warn("cause:  {} ", t.getMessage());
              getLogger().warn("stack trace:  {}", ExceptionUtils.getStackTrace(t));
            }
        )
        .doOnError(t -> {
              if (!fullIndexEnabled || (fullIndexEnabled && batchValidationPoll>0)) {
                if (t instanceof BadRequestException || t instanceof NonRetryableIntegrationException || t instanceof CommitFailedException) {
                  scheduler.dispose();
                  backoffScheduler.dispose();
                  consume();
                } else {
                  getLogger().warn("cause {} :  {} ", t.getClass(), t.getMessage());
                  getLogger().info("Disposing consumer after maximum retries..");
                  scheduler.dispose();
                  backoffScheduler.dispose();
                  getLogger().info("Starting Remote server Monitoring Service...");
                  waitForCircuitClose(getHealthUrl(t), t);
                }
              }
            }
        )
        .doOnTerminate(() -> getLogger().info("Terminating {}...", Thread.currentThread().getName()))
        .subscribe();
  }

  private boolean checkIfExceptionTobeLogged(Throwable exception) {
    return !(exception instanceof RetryExhaustedException && exception.getCause() instanceof ConnectException) && !(exception instanceof ConnectException)
        && !(exception instanceof AlgoliaRuntimeException);
  }

  private String getHealthUrl(Throwable throwable) {
    String url = null;
    if (throwable instanceof RetryableIntegrationException) {
      url = ((RetryableIntegrationException) throwable).getUrl() + "/actuator/health";
    } else if (throwable instanceof RetryExhaustedException) {
      if (throwable.getMessage().contains("Connection refused")) {
        url = throwable.getMessage() + "/actuator/health";
      }
    } else if (throwable instanceof AlgoliaRuntimeException) {
      url = "https://" + applicationKey + healthStatusUrl;
    }
    return url;
  }


  protected void waitForCircuitClose(String url, Throwable t) {
    AtomicBoolean health = new AtomicBoolean(false);
    final Disposable subscribed;
    if (t instanceof AlgoliaRuntimeException) {
      subscribed = monitorAlgoliaApiServerHealth(url)
          .doOnNext(x -> {
            if (x.getItems() != null || x.getStatus().equalsIgnoreCase("200")) {
              health.set(true);
            } else {
              getLogger().warn("Remote Server is yet not reachable");
            }
          })
          .subscribe();
    } else {
      subscribed = monitorApiServerHealth(url)
          .doOnNext(x -> {
            if (x.getCode().contentEquals("UP")) {
              health.set(true);
            } else {
              getLogger().warn("Remote Server is yet not reachable");
            }
          })
          .subscribe();
    }

    while (!health.get()) {
      getLogger().error("Monitoring remote server health.");
    }
    getLogger().info("Remote Server is reachable,disposing monitoring");
    subscribed.dispose();
    getLogger().info("Recreating Consumer...");
    consume();
  }

  private Flux<Status> monitorApiServerHealth(String url) {
    UriComponents uriComponents = UriComponentsBuilder
        .fromUriString(url).build();
    HealthDto fallback = new HealthDto();
    fallback.setStatus(Status.UNKNOWN);
    return Flux
        .interval(Duration.ofSeconds(circuitOpenInterval))
        .flatMap(i -> webClient
            .method(HttpMethod.GET)
            .uri(uriComponents.toUri())
            .contentType(MediaType.TEXT_EVENT_STREAM)
            .retrieve()
            .bodyToMono(HealthDto.class)
            .onErrorReturn(fallback)
            .map(HealthDto::getStatus)
        );
  }

  private Flux<HealthAlgolia> monitorAlgoliaApiServerHealth(String url) {
    UriComponents uriComponents = UriComponentsBuilder
        .fromUriString(url).build();
    HealthAlgolia fallback = new HealthAlgolia();
    fallback.setStatus(Status.UNKNOWN.getCode());
    return Flux
        .interval(Duration.ofSeconds(circuitOpenInterval))
        .flatMap(i -> webClient
            .method(HttpMethod.GET)
            .uri(uriComponents.toUri())
            .contentType(MediaType.TEXT_EVENT_STREAM)
            .header("X-Algolia-API-Key", adminApiKey)
            .header("X-Algolia-Application-Id", applicationKey)
            .retrieve()
            .bodyToMono(HealthAlgolia.class)
            .onErrorReturn(fallback)
        );
  }
  protected abstract Flux<List<ReceiverRecord<K, V>>> publish(List<ReceiverRecord<K, V>> receivedRecords, PublishOperation publishOperation,
      Boolean dlqPublishEnabled, String dlqTopicName, Scheduler dlqSenderScheduler, KafkaSender<K, V> dlqSender,
      MeterRegistry meterRegistry);

  private void acknowledge(List<ReceiverRecord<K,V>> receivedRecords) {
    receivedRecords.stream().forEach(receivedRecord -> {
      if(receivedRecord.key() instanceof KafkaKey && ((KafkaKey) receivedRecord.key()).isDlqPublish()){
        handleError(receivedRecord, config.isDlqPublishEnabled(), generateDLQTopicName(), dlqSenderScheduler, dlqSender, meterRegistry,
            ((KafkaKey) receivedRecord.key()).getThrowable());
      }else {
        ReceiverOffset offset = receivedRecord.receiverOffset();
        getLogger().info("Offset acknowledged: {}", offset);
        offset.acknowledge();
      }
    });

  }

  private void acknowledge(ReceiverRecord<K, V> receivedRecord) {
    ReceiverOffset offset = receivedRecord.receiverOffset();
    getLogger().info("Offset acknowledged: {}", offset);
    offset.acknowledge();
  }

  protected String getRequestBody(ReceiverRecord<K, V> receivedRecord) throws JsonProcessingException {
    return receivedRecord.value() != null ? objectMapper.writeValueAsString(receivedRecord.value()) : null;
  }

  protected void configureConsumerProperties(Map<String, Object> consumerProps, String groupId) {
    consumerProps.put(KEY_DEFAULT_TYPE, getKeyClass());
    consumerProps.put(VALUE_DEFAULT_TYPE, getRequestClass());
    if (groupId != null) {
      consumerProps.put(GROUP_ID_CONFIG, groupId);
    }
  }


  protected String generateTopicName() {
    return getTopicName();
  }

  protected String generateDLQTopicName() {
    String topicName = HYPHEN.equals(topicNameDelimiter) ? getTopicName() : getTopicName().replace(HYPHEN, topicNameDelimiter);
    String dlqTopicName = String.join(topicNameDelimiter, topicName, deadLetterQueueSuffix);
    return env.getProperty(OVERRIDE_KAFKA_TOPIC_NAME_PROPERTY_PREFIX + dlqTopicName, dlqTopicName);
  }



  protected void handleError(ReceiverRecord<K, V> receivedRecord, boolean dlqPublishEnabled, String dlqTopicName, Scheduler dlqSenderScheduler, KafkaSender<K, V> dlqSender,
      MeterRegistry meterRegistry, Throwable t) {

    getLogger().error("Unable to process record:{} {} " , receivedRecord, t);
    if (dlqPublishEnabled) {

      Headers headers = new RecordHeaders(Arrays.stream(receivedRecord.headers().toArray())
          .collect(Collectors.toList()));

      Optional<String> optionalStatusCode = Optional.ofNullable(t)
          .map(error -> {
            if (error instanceof NonRetryableIntegrationException) {
              return ((NonRetryableIntegrationException) error).getHttpStatus();
            } else if (error instanceof RetryableIntegrationException) {
              return ((RetryableIntegrationException) error).getHttpStatus();
            } else if (error instanceof BadRequestException) {
              return ((BadRequestException) error).getHttpStatus();
            } else {
              return null;
            }
          })
          .map(HttpStatus::value)
          .map(String::valueOf);

      optionalStatusCode.map(String::getBytes)
          .ifPresent(value -> headers.add(new RecordHeader(ERROR_CODE, value)));

      String statusTag = optionalStatusCode.orElse("OTHER");
      meterRegistry.counter(dlqTopicName, STATUS_CODE, statusTag).increment();

      Optional.ofNullable(t.getMessage())
          .map(String::getBytes)
          .ifPresent(value -> headers.add(new RecordHeader(ERROR_MESSAGE, value)));

      Mono<SenderRecord<K, V, K>> recordsToSend = Mono.just(SenderRecord.create(
          new ProducerRecord<>(dlqTopicName, null, receivedRecord.timestamp(), receivedRecord.key(),
              receivedRecord.value(), headers), receivedRecord.key()));

      dlqSender.send(recordsToSend)
          .subscribeOn(dlqSenderScheduler)
          .doOnError(e -> getLogger().error("Error while producing into topic {}", dlqTopicName, e))
          .doOnNext(r -> getLogger().debug("Message {} send response: {}", r.correlationMetadata(), r.recordMetadata()))
          .then(Mono.empty())
          .subscribe();

      if (t instanceof BadRequestException || t instanceof NonRetryableIntegrationException) {
        acknowledge(receivedRecord);
      }

    }
  }

  public abstract String getTopicName();

  public abstract Logger getLogger();

  public abstract Class<K> getKeyClass();

  public abstract Class<V> getRequestClass();

}
