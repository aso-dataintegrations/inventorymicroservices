package com.yantriks.academy.streamer.consumer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.yantriks.academy.dto.KafkaAtpPublish;
import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import com.yantriks.academy.dto.KafkaAtpPublishByProduct;
import com.yantriks.academy.dto.KafkaAtpPublishBySegment;
import com.yantriks.academy.dto.KafkaAtpPublishEntity;
import com.yantriks.academy.dto.OperationType;
import com.yantriks.academy.dto.key.KafkaProductKey;
import com.yantriks.academy.streamer.service.util.EmbeddedMockServer;

import reactor.kafka.receiver.ReceiverRecord;

@WebFluxTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "algolia")
@ContextConfiguration( classes = {
		EmbeddedMockServer.class,
		NetworkAtpThresholdBatchAlgoliaIntegration.class,
		ReceiverRecord.class,
		KafkaProductKey.class,
		KafkaAtpPublishEntity.class
})
class NetworkAtpThresholdBatchAlgoliaIntegrationTest {

	@InjectMocks
	NetworkAtpThresholdBatchAlgoliaIntegration networkAtpThresholdBatchAlgoliaIntegration;
	List<ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>> receiverRecords;
	String topic = "aso-product-atp-threshold-pub";
	
	@MockBean
	ReceiverRecord receiverRecord;

	@BeforeEach
	void setup(){
		receiverRecords = this.getReceiverRecords();
	}
	
	//removeDuplicateReceiverRecordfromReceiverRecordListTest
	@Test
	void duplicateReceiverRecordTest() {
		networkAtpThresholdBatchAlgoliaIntegration.removeDuplicateReceiverRecordfromReceiverRecordList(receiverRecords, getReceiverRecordToValidateDuplicacy());
		Assertions.assertSame(1, receiverRecords.size());
	}
	
	@Test
	void nonDuplicateReceiverRecordTest() {
		networkAtpThresholdBatchAlgoliaIntegration.removeDuplicateReceiverRecordfromReceiverRecordList(receiverRecords, getReceiverRecordToValidateNonDuplicacy());
		Assertions.assertSame(2, receiverRecords.size());
	}
	
	public List<ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>> getReceiverRecords() {
		List<ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>> receiverRecords = new ArrayList<>();
		long timestamp = System.currentTimeMillis();

		ConsumerRecord consumerRecord = new ConsumerRecord<KafkaProductKey, KafkaAtpPublishEntity>(topic, 1, 1, getKafkaProductKey("ACADEMY", "128655680", "EACH", "SHIP"), getKafkaAtpPublishEntity("SHIP", "2024-04-03T06:42:37.084"));
		receiverRecords.add(new ReceiverRecord<>(consumerRecord, null));

		return receiverRecords;
	}

	public ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> getReceiverRecordToValidateDuplicacy() {
		return this.getReceiverRecord(topic, 2, 2, "ACADEMY", "128655680", "EACH", "SHIP", "2024-04-03T06:43:37.084");
	}	
	
	public ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> getReceiverRecordToValidateNonDuplicacy() {
		return this.getReceiverRecord(topic, 2, 2, "ACADEMY", "128655680", "EACH", "SHIP", "2024-04-03T06:40:37.084");
	}

	public ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> getReceiverRecord(String topic, int partition, long offset, String orgId, String productId, String uom, String fulfillmentType, String localDateTime) {
		ConsumerRecord consumerRecord = new ConsumerRecord<KafkaProductKey, KafkaAtpPublishEntity>(topic, partition, offset, getKafkaProductKey(orgId, productId, uom, fulfillmentType), getKafkaAtpPublishEntity(fulfillmentType, localDateTime));
		return new ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>(consumerRecord, null);
	}
		 
	public KafkaProductKey getKafkaProductKey(String orgId, String productId, String uom, String fulfillmentType) {
		KafkaProductKey kafkaProductKey = new KafkaProductKey(orgId, productId, uom, fulfillmentType);
		return kafkaProductKey;
	}

	public KafkaAtpPublishEntity getKafkaAtpPublishEntity(String fulfillmentType, String localDateTime) {
		KafkaAtpPublishEntity kafkaAtpPublishEntity = new KafkaAtpPublishEntity();
		kafkaAtpPublishEntity.setOperationType(OperationType.UPSERT);
		kafkaAtpPublishEntity.setUpdateTime(getLocalDateTime(localDateTime));

		KafkaAtpPublish entity = new KafkaAtpPublish();
		entity.setOrgId("ACADEMY");
		entity.setSellingChannel("ACADEMY.COM");
		entity.setTransactionType("WCS");

		KafkaAtpPublishByProduct kafkaAtpPublishByProduct = new KafkaAtpPublishByProduct();
		kafkaAtpPublishByProduct.setProductId("128655680");
		kafkaAtpPublishByProduct.setUom("EACH");		


		KafkaAtpPublishByFT kafkaAtpPublishByFT = new KafkaAtpPublishByFT();		
		KafkaAtpPublishBySegment kafkaAtpPublishBySegment = new KafkaAtpPublishBySegment();
		kafkaAtpPublishBySegment.setSegment("DEFAULT");		
		kafkaAtpPublishBySegment.setAtpStatus("OOS");
		kafkaAtpPublishBySegment.setOldAtpStatus("IN_STOCK");
		kafkaAtpPublishBySegment.setAtp(0.0);
		kafkaAtpPublishBySegment.setDemand(107.0);
		kafkaAtpPublishBySegment.setSupply(0.0);
		kafkaAtpPublishBySegment.setSafetyStock(0.0);
		Set<KafkaAtpPublishBySegment> availabilityDetails = new HashSet();
		availabilityDetails.add(kafkaAtpPublishBySegment);

		Set<KafkaAtpPublishByFT> availabilityByFulfillmentTypes = new HashSet<>();
		availabilityByFulfillmentTypes.add(kafkaAtpPublishByFT);


		kafkaAtpPublishByProduct.setAvailabilityByFulfillmentTypes(availabilityByFulfillmentTypes);
		kafkaAtpPublishByFT.setAvailabilityDetails(availabilityDetails);
		kafkaAtpPublishByFT.setFulfillmentType(fulfillmentType);

		Set<KafkaAtpPublishByProduct> availabilityByProducts = new HashSet<>();
		availabilityByProducts.add(kafkaAtpPublishByProduct);
		entity.setAvailabilityByProducts(availabilityByProducts);

		kafkaAtpPublishEntity.setEntity(entity);

		return kafkaAtpPublishEntity;		

	}

	public LocalDateTime getLocalDateTime(String time) {
		return LocalDateTime.parse(time);
	}
}