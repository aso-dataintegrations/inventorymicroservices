package com.yantriks.academy.streamer.service.util;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.AVAILABILITY_STATUS_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LSI;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LSI_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK_INV_PICK_PREFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_AVAILABILITY_STATUS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_INV_UPDTAE_FLAG;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_QUANTITY;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_AVAILABILITY_STATUS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_INV_UPDTAE_FLAG;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_QUANTITY;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS_ID_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS_PREFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.UNDERSCORE;

import com.yantriks.academy.dto.InventoryMap;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class TestMapperUtil {

  public Map<String,Object> createLsiInventoryMap(String locationId, Double atp) {
    InventoryMap map = new InventoryMap();
    map.put(LSI + UNDERSCORE + locationId, atp);
    map.put(LSI_INDEX_TIME, Calendar.getInstance().getTimeInMillis());
    return map;
  }
  public Map<String,Object> createSthInventoryMap(String locationId, Double atp, String atpStatus) {
    InventoryMap map = new InventoryMap();
    map.put(STH_INDEX_TIME, Calendar.getInstance().getTimeInMillis());
    map.put(STH_INV_UPDTAE_FLAG, atp > 0 ? 'Y' : 'N');
    map.put(STH_AVAILABILITY_STATUS, atpStatus);
    map.put(STH_QUANTITY, atp);
    return map;
  }

  public Map<String,Object> createStsfsInventoryMap(String locationId, Double atp, String atpStatus) {
    InventoryMap map = new InventoryMap();
    map.put(STSFS_INDEX_TIME, Calendar.getInstance().getTimeInMillis());
    map.put(STSFS_INV_UPDTAE_FLAG, atp > 0 ? 'Y' : 'N');
    map.put(STSFS_AVAILABILITY_STATUS, atpStatus);
    map.put(STSFS_QUANTITY, atp);
    return map;
  }

  public Map<String,Object> createPickInventoryMap(String locationId, Double atp, String atpStatus) {
    InventoryMap map = new InventoryMap();
    map.put(PICK_INV_PICK_PREFIX + locationId, atp);
    map.put(PICK_INV_PICK_PREFIX + locationId + AVAILABILITY_STATUS_SUFFIX, atpStatus);
    map.put(PICK_INDEX_TIME, Calendar.getInstance().getTimeInMillis());
    return map;
  }
  public Map<String,Object> createStsInventoryMap(String locationId, Double atp, String atpStatus, Map<String, List<String>> stsMap) {
    InventoryMap map = new InventoryMap();
    for (String store : stsMap.get(locationId)) {
      map.put(STS_PREFIX + store, atp);
      map.put(STS_PREFIX + store + STS_ID_SUFFIX, locationId);
      map.put(STS_PREFIX + store + AVAILABILITY_STATUS_SUFFIX, atpStatus);
    }
    return map;
  }
}
