package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.AVAILABILITY_STATUS_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LIMITEDSTOCK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LIMITED_STOCK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LSI;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.UNDERSCORE;
import static org.mockito.Mockito.when;

import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import com.yantriks.academy.dto.KafkaAtpPublishBySegment;
import com.yantriks.academy.streamer.schedulers.DcToStoreMappingScheduler;
import com.yantriks.academy.streamer.service.util.EmbeddedMockServer;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@WebFluxTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "algolia")
@ContextConfiguration( classes = {
    EmbeddedMockServer.class
})
class StsMapperTest {
  @Mock
  DcToStoreMappingScheduler dcToStoreMappingScheduler;
  @InjectMocks
  StsMapper stsMapper;
  KafkaAtpPublishByFT availabilityByFulfillmentTypes;
  String locationId;
  Double atp;
  Map<String, List<String>> stsMap;

  @BeforeEach
  void setup(){
    locationId = "001";
    dcToStoreMappingScheduler.init();
    stsMap = new HashMap<>();
    stsMap.put("1", Arrays.asList("033","101"));
    when(dcToStoreMappingScheduler.getCache()).thenReturn(stsMap);
    atp = 100.0;
    Set<KafkaAtpPublishBySegment> kafkaAtpPublishBySegmentSet = new HashSet<>();
    kafkaAtpPublishBySegmentSet.add(KafkaAtpPublishBySegment.builder().atp(atp).atpStatus(LIMITED_STOCK).build());
    availabilityByFulfillmentTypes = KafkaAtpPublishByFT.builder()
        .fulfillmentType(STS).availabilityDetails(kafkaAtpPublishBySegmentSet).build();

  }
  @Test
  void mapRecordsSuccess(){
    Map<String,Object> result = stsMapper.mapRecords(locationId, LocalDateTime.now()
        ,availabilityByFulfillmentTypes);
    Assertions.assertNotNull(result);
    Assertions.assertEquals(LIMITEDSTOCK,result.get(STS+UNDERSCORE+"033"+AVAILABILITY_STATUS_SUFFIX));
  }
}
