package com.yantriks.academy.streamer.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.yantriks.academy.streamer.schedulers.DcToStoreMappingScheduler;
import com.yantriks.academy.streamer.service.util.EmbeddedMockServer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@WebFluxTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "algolia")
@ContextConfiguration(classes = {
    EmbeddedMockServer.class,
    DcToStoreMappingScheduler.class
})
class DcToStoreMappingControllerTest {

  @InjectMocks
  DcToStoreMappingController dcToStoreMappingController;

  @Mock
  DcToStoreMappingScheduler dcToStoreMappingScheduler;
  Map<String, List<String>> stsMap;

  @BeforeEach
  public void setUp() {
    stsMap = new HashMap<>();
    stsMap.put("1", Arrays.asList("033", "101"));
    when(dcToStoreMappingScheduler.getCache()).thenReturn(stsMap);

  }

  @Test
  void getResponseSuccess() {
    Assertions.assertEquals(stsMap,dcToStoreMappingController.getResponse());
  }

}
