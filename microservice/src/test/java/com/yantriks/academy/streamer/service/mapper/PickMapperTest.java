package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.AVAILABILITY_STATUS_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.AVAILABLE;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.IN_STOCK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LSI;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.UNDERSCORE;

import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import com.yantriks.academy.dto.KafkaAtpPublishBySegment;
import com.yantriks.academy.streamer.service.util.EmbeddedMockServer;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@WebFluxTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "algolia")
@ContextConfiguration( classes = {
    EmbeddedMockServer.class
})
class PickMapperTest {
  @InjectMocks
  PickMapper pickMapper;
  KafkaAtpPublishByFT availabilityByFulfillmentTypes;
  String locationId;
  Double atp;

  @BeforeEach
  void setup(){
    locationId = "033";
    atp = 100.0;
    Set<KafkaAtpPublishBySegment> kafkaAtpPublishBySegmentSet = new HashSet<>();
    kafkaAtpPublishBySegmentSet.add(KafkaAtpPublishBySegment.builder().atp(atp).atpStatus(IN_STOCK).build());
    availabilityByFulfillmentTypes = KafkaAtpPublishByFT.builder()
        .fulfillmentType(PICK).availabilityDetails(kafkaAtpPublishBySegmentSet).build();

  }
  @Test
  void mapRecordsSuccess(){
    Map<String,Object> result = pickMapper.mapRecords(locationId, LocalDateTime.now(),availabilityByFulfillmentTypes);
    Assertions.assertNotNull(result);
    Assertions.assertEquals(AVAILABLE,result.get(PICK+UNDERSCORE+locationId+AVAILABILITY_STATUS_SUFFIX));
  }
}
