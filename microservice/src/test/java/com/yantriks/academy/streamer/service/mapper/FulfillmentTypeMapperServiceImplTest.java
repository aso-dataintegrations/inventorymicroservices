package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.AVAILABILITY_STATUS_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LSI;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LSI_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK_INV_PICK_PREFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_AVAILABILITY_STATUS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_INV_UPDTAE_FLAG;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_QUANTITY;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STORE;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_AVAILABILITY_STATUS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_INV_UPDTAE_FLAG;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_QUANTITY;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS_ID_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS_PREFIX;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.yantriks.academy.dto.InventoryMap;
import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import com.yantriks.academy.dto.KafkaAtpPublishByProduct;
import com.yantriks.academy.dto.KafkaAtpPublishBySegment;
import com.yantriks.academy.streamer.config.MetricsConfig;
import com.yantriks.academy.streamer.kafka.configuration.RestApiConfigurationProperties;
import com.yantriks.academy.streamer.schedulers.DcToStoreMappingScheduler;
import com.yantriks.academy.streamer.service.util.EmbeddedMockServer;
import com.yantriks.academy.streamer.service.util.TestMapperUtil;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;

@WebFluxTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "algolia")
@ContextConfiguration( classes = {
   RestApiConfigurationProperties.class,
    MetricsConfig.class,
    WebClient.class,
    EmbeddedMockServer.class,
    DcToStoreMappingScheduler.class,
    LsiMapper.class,
    PickMapper.class,
    ShipMapper.class,
    StsMapper.class,
    TestMapperUtil.class,
    FulfillmentTypeMapperServiceImpl.class
})
class FulfillmentTypeMapperServiceImplTest {


  @Mock
  DcToStoreMappingScheduler dcToStoreMappingScheduler;

  @InjectMocks
  FulfillmentTypeMapperServiceImpl fulfillmentTypeMapperServiceImpl;
  @Mock
  LsiMapper lsiMapper;
  @Mock
  ShipMapper shipMapper;
  @Mock
  ShipMapper stsfsMapper;
  @Mock
  PickMapper pickMapper;
  @Mock
  StsMapper stsMapper;

  @Autowired
  TestMapperUtil testMapperUtil;

  KafkaAtpPublishByProduct availabilityByProducts;
  List<String> fulfillmentTypeList;
  Double atp;
  String locationId;
  String atpStatus;
  String fulfillmentType;
  Map<String, List<String>> stsMap;

  @BeforeEach
  void init(){
    dcToStoreMappingScheduler.init();
    stsMap = new HashMap<>();
    stsMap.put("1",Arrays.asList("033","101"));
    when(dcToStoreMappingScheduler.getCache()).thenReturn(stsMap);
    atp=100.0;
    locationId="033";
    atpStatus="IN_STOCK";
    fulfillmentTypeList = new ArrayList<>();
    fulfillmentTypeList.addAll(Arrays.asList(LSI,STH,STSFS,STS,PICK));

  }

  @Test
  void processRecordByFulfillmentTypeLSI(){
    this.fulfillmentType=LSI;
    setUpAvailabilityTestData();
    Map<String,Object> expected = testMapperUtil.createLsiInventoryMap(locationId,atp);
   when(lsiMapper.mapRecords(any(),any(),any())).thenReturn(expected);
   Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType
       (STORE, LocalDateTime.now(),
           "033",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertSame(expected.get("LSI_"+033),result.get("LSI_"+033));
    Assertions.assertSame(expected.get("LSI_INDEX_TIME"),result.get("LSI_INDEX_TIME"));

  }


  @Test
  void processRecordByFulfillmentTypeLSIWhenNotInEligibleFT(){
    this.fulfillmentType=LSI;
    setUpAvailabilityTestData();
    fulfillmentTypeList.remove(fulfillmentType);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType
        (STORE, LocalDateTime.now(),
            "033",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertTrue(result.isEmpty());
  }


  @Test
  void processRecordByFulfillmentTypeSTH(){
    this.fulfillmentType=STH;
    setUpAvailabilityTestData();
    Map<String,Object> expected = testMapperUtil.createSthInventoryMap(locationId,atp,atpStatus);
    when(shipMapper.mapRecords(any(),any(),any())).thenReturn(expected);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType(null, LocalDateTime.now(),
            null,new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertSame(expected.get(STH_INDEX_TIME), result.get(STH_INDEX_TIME));
    Assertions.assertSame(expected.get(STH_INV_UPDTAE_FLAG), result.get(STH_INV_UPDTAE_FLAG));
    Assertions.assertSame(expected.get(STH_AVAILABILITY_STATUS), result.get(STH_AVAILABILITY_STATUS));
    Assertions.assertSame(expected.get(STH_QUANTITY), result.get(STH_QUANTITY));
  }

  @Test
  void processRecordByFulfillmentTypeSTHWhenNotInEligibleFT(){
    this.fulfillmentType=STH;
    setUpAvailabilityTestData();
    fulfillmentTypeList.remove(fulfillmentType);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType(null, LocalDateTime.now(),
            "033",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertTrue(result.isEmpty());
  }

  @Test
  void processRecordByFulfillmentTypeSTSFS(){
    this.fulfillmentType=STSFS;
    setUpAvailabilityTestData();
    Map<String,Object> expected = testMapperUtil.createStsfsInventoryMap(locationId,atp,atpStatus);
    when(stsfsMapper.mapRecords(any(),any(),any())).thenReturn(expected);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType(null, LocalDateTime.now(),
        null,new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertSame(expected.get(STSFS_INDEX_TIME), result.get(STSFS_INDEX_TIME));
    Assertions.assertSame(expected.get(STSFS_INV_UPDTAE_FLAG), result.get(STSFS_INV_UPDTAE_FLAG));
    Assertions.assertSame(expected.get(STSFS_AVAILABILITY_STATUS), result.get(STSFS_AVAILABILITY_STATUS));
    Assertions.assertSame(expected.get(STSFS_QUANTITY), result.get(STSFS_QUANTITY));
  }

  @Test
  void processRecordByFulfillmentTypeSTSFSWhenNotInEligibleFT(){
    this.fulfillmentType=STSFS;
    setUpAvailabilityTestData();
    fulfillmentTypeList.remove(fulfillmentType);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType(null, LocalDateTime.now(),
        "033",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertTrue(result.isEmpty());
  }

  @Test
  void processRecordByFulfillmentTypePick(){
    this.fulfillmentType=PICK;
    setUpAvailabilityTestData();
    Map<String,Object> expected = testMapperUtil.createPickInventoryMap(locationId,atp,atpStatus);
    when(pickMapper.mapRecords(any(),any(),any())).thenReturn(expected);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType(STORE, LocalDateTime.now(),
        "033",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertSame(expected.get(PICK_INV_PICK_PREFIX+"033"), result.get(PICK_INV_PICK_PREFIX+"033"));
    Assertions.assertSame(expected.get(PICK_INV_PICK_PREFIX + "033" + AVAILABILITY_STATUS_SUFFIX), result.get(PICK_INV_PICK_PREFIX + "033" + AVAILABILITY_STATUS_SUFFIX));
    Assertions.assertSame(expected.get(PICK_INDEX_TIME), result.get(PICK_INDEX_TIME));

  }

  @Test
  void processRecordByFulfillmentTypePickWhenNotInEligibleFT(){
    this.fulfillmentType=PICK;
    setUpAvailabilityTestData();
    fulfillmentTypeList.remove(fulfillmentType);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType(STORE, LocalDateTime.now(),
        "033",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertTrue(result.isEmpty());
  }

  @Test
  void processRecordByFulfillmentTypeSTS(){
    this.fulfillmentType=STS;
    locationId="1";
    setUpAvailabilityTestData();
    Map<String,Object> expected = testMapperUtil.createStsInventoryMap(locationId, atp, atpStatus, stsMap);
    when(stsMapper.mapRecords(any(),any(),any())).thenReturn(expected);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType("DC", LocalDateTime.now(),
        "001",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertSame(expected.get(STS_PREFIX+"033"), result.get(STS_PREFIX+"033"));
    Assertions.assertSame(expected.get(STS_PREFIX + "033" + STS_ID_SUFFIX), result.get(STS_PREFIX + "033" + STS_ID_SUFFIX));
    Assertions.assertSame(expected.get(STS_PREFIX + "033" + AVAILABILITY_STATUS_SUFFIX), result.get(STS_PREFIX + "033" + AVAILABILITY_STATUS_SUFFIX));
  }

  @Test
  void processRecordByFulfillmentTypeStsWhenNotInEligibleFT(){
    this.fulfillmentType=STS;
    setUpAvailabilityTestData();
    fulfillmentTypeList.remove(fulfillmentType);
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType("DC", LocalDateTime.now(),
        "001",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertTrue(result.isEmpty());
  }
  @Test
  void processRecordByFulfillmentTypeStsWhenLocationTypeisStore(){
    this.fulfillmentType=STS;
    setUpAvailabilityTestData();
    Map<String,Object> result = fulfillmentTypeMapperServiceImpl.processRecordByFulfillmentType("STORE", LocalDateTime.now(),
        "001",new InventoryMap(),availabilityByProducts,fulfillmentTypeList);
    Assertions.assertTrue(result.isEmpty());
  }


  private void setUpAvailabilityTestData() {
    availabilityByProducts = new KafkaAtpPublishByProduct();
    availabilityByProducts.setProductId("test1");
    Set<KafkaAtpPublishBySegment> kafkaAtpPublishBySegmentSet= new HashSet<>();
    kafkaAtpPublishBySegmentSet.add(KafkaAtpPublishBySegment.builder().atp(atp).atpStatus(atpStatus).build());
    Set<KafkaAtpPublishByFT> kafkaAtpPublishByFTSet = new HashSet<>();
    kafkaAtpPublishByFTSet.add(KafkaAtpPublishByFT.builder()
        .fulfillmentType(fulfillmentType).availabilityDetails(kafkaAtpPublishBySegmentSet).build());
    availabilityByProducts.setAvailabilityByFulfillmentTypes(kafkaAtpPublishByFTSet);
  }


}
