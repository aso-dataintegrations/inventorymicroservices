package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.NOTAVAILABLE;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.OOS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_AVAILABILITY_STATUS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_OOSTIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_AVAILABILITY_STATUS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_OOSTIME;

import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import com.yantriks.academy.dto.KafkaAtpPublishBySegment;
import com.yantriks.academy.streamer.service.util.EmbeddedMockServer;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@WebFluxTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "algolia")
@ContextConfiguration( classes = {
    EmbeddedMockServer.class
})
class StsfsMapperTest {
  @InjectMocks
  StsfsMapper stsfsMapper;
  KafkaAtpPublishByFT availabilityByFulfillmentTypes;
  String locationId;
  Double atp;

  @BeforeEach
  void setup(){
    locationId = "033";
    atp = 100.0;
    Set<KafkaAtpPublishBySegment> kafkaAtpPublishBySegmentSet = new HashSet<>();
    kafkaAtpPublishBySegmentSet.add(KafkaAtpPublishBySegment.builder().atp(atp).atpStatus(OOS).build());
    availabilityByFulfillmentTypes = KafkaAtpPublishByFT.builder()
        .fulfillmentType(STSFS).availabilityDetails(kafkaAtpPublishBySegmentSet).build();

  }
  @Test
  void mapRecordsSuccess(){
    Map<String,Object> result = stsfsMapper.mapRecords(locationId, LocalDateTime.now(),availabilityByFulfillmentTypes);
    Assertions.assertNotNull(result);
    Assertions.assertEquals('N',result.get(STSFS_OOSTIME));
  }
  @Test
  void mapRecordsSuccessWithAtpZero(){
    LocalDateTime updateTime = LocalDateTime.now();
    atp = 0.0;
    availabilityByFulfillmentTypes.getAvailabilityDetails().stream().findFirst().get().setAtp(atp);
    Map<String,Object> result = stsfsMapper.mapRecords(locationId, updateTime,availabilityByFulfillmentTypes);
    Assertions.assertNotNull(result);
    Assertions.assertEquals(updateTime,result.get(STSFS_OOSTIME));
    Assertions.assertEquals(NOTAVAILABLE,result.get(STSFS_AVAILABILITY_STATUS));
  }

}
