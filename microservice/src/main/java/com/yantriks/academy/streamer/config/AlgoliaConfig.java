package com.yantriks.academy.streamer.config;

import com.algolia.search.DefaultSearchClient;
import com.algolia.search.SearchClient;
import com.algolia.search.SearchConfig;
import com.algolia.search.SearchIndex;
import com.yantriks.academy.dto.InventoryMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Slf4j
@Configuration
public class AlgoliaConfig {

    // application key
    @Value("${algolia.application_key}")
    private String applicationKey;
    // admin api key
    @Value("${algolia.admin_api_key}")
    private String adminApiKey;
    // connectTimeout
    @Value("${algolia.connection-timeout}")
    private Integer connectTimeout;
    // readTimeout
    @Value("${algolia.read-timeout}")
    private Integer readTimeout;
    // writeTimeout
    @Value("${algolia.write-timeout}")
    private Integer writeTimeout;
    //index name
    @Value("${algolia.index-name}")
    private String indexName;

    /*
      create default search client for algolia with all configuration.
     */
    @Bean
    @Qualifier("inventoryIndex")
    SearchIndex<InventoryMap> inventoryIndex() {
        SearchConfig configuration =
                new SearchConfig.Builder(applicationKey, adminApiKey)
                        .setConnectTimeOut(connectTimeout) // connection timeout in milliseconds
                        .setReadTimeOut(readTimeout) // read timeout in milliseconds
                        .setWriteTimeOut(writeTimeout) // write timeout in milliseconds
                        .build();
        SearchClient client = DefaultSearchClient.create(configuration);
        return client.initIndex(indexName, InventoryMap.class);
    }
}
