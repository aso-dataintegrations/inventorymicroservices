package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.AVAILABILITY_STATUS_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.NOTAVAILABLE;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.OOS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK_INV_PICK_PREFIX;

import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import com.yantriks.academy.dto.KafkaAtpPublishBySegment;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile("algolia")
@Qualifier("PICK")
@EnableConfigurationProperties
public class PickMapper extends Mapper{
  /*
   maps consumer records for fulfillmentType PICK
    */
  @Override
  public Map<String, Object> mapRecords(String locationId, LocalDateTime updateTime, KafkaAtpPublishByFT availabilityByFulfillmentTypes) {
    Map<String, Object> inventoryMap = new HashMap<>();
    for (KafkaAtpPublishBySegment atpPublishBySegment : availabilityByFulfillmentTypes.getAvailabilityDetails()) {
      Double atp = atpPublishBySegment.getAtp();
      inventoryMap.put(PICK_INV_PICK_PREFIX + locationId, atp);
      inventoryMap.put(PICK_INV_PICK_PREFIX + locationId + AVAILABILITY_STATUS_SUFFIX,  getValidAtpStatus(atpPublishBySegment.getAtpStatus()));
      inventoryMap.put(PICK_INDEX_TIME, Calendar.getInstance().getTimeInMillis());
    }
    return inventoryMap;
  }
}
