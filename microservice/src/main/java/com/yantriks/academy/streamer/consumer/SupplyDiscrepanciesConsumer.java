package com.yantriks.academy.streamer.consumer;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yantriks.academy.dto.OperationType;
import com.yantriks.academy.dto.supply_discrepancies.KafkaSupplyDiscrepanciesValue;
import com.yantriks.academy.dto.supply_discrepancies.SupplyDiscrepanciesPublishEntity;
import com.yantriks.academy.dto.supply_discrepancies.key.KafkaSupplyDiscrepanciesKey;
import com.yantriks.academy.streamer.kafka.constants.HealthDto;
import com.yantriks.academy.streamer.kafka.consumers.AbstractKafkaRestIntegration;
import com.yantriks.academy.streamer.kafka.exceptions.BadRequestException;
import com.yantriks.academy.streamer.kafka.exceptions.NonRetryableIntegrationException;
import com.yantriks.academy.streamer.kafka.exceptions.RetryableIntegrationException;
import com.yantriks.academy.streamer.service.mapper.SupplyDiscrepancyMapperService;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import com.yantriks.academy.streamer.kafka.tokengenerator.GenerateJWTToken;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.kafka.receiver.ReceiverRecord;
import reactor.kafka.sender.KafkaSender;
import reactor.retry.Retry;
import java.net.ConnectException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


@Slf4j
@Component
@Profile("supply-discrepancies")
@EnableConfigurationProperties
public class SupplyDiscrepanciesConsumer extends AbstractKafkaRestIntegration<KafkaSupplyDiscrepanciesKey, KafkaSupplyDiscrepanciesValue, Void> {

    @Value("${api.server.yantriks-kafka-rest-services}")
    private String supDisRestUrl;

    @Value("${api.server.yantriks-kafka-health-check-url}")
    private String yantriksKafkaRestHealthCheck;

    @Value("#{'${supply-discrepancy.dc-locations}'.split(',')}")
    private List<String> dcLocations;

    @Value("${streamer.aso-inv-sup-discrep-pub}")
    private String topicName;

    @Value("${publish-topic.yantriks-supply-discrepancies}")
    private String publishTopicName;

    @Value("${kafka.consumer.circuit-open.interval-in-sec:5}")
    private int circuitOpenInterval;


    @Autowired
    private WebClient webClient;

    @Autowired
    private ObjectMapper objectMapper;

    String jwtToken;

    @Autowired
    private GenerateJWTToken generateJWTToken;

    @Autowired
    SupplyDiscrepancyMapperService supplyDiscrepancyMapperService;


    @Override
    protected Mono<Void> publish(ReceiverRecord<KafkaSupplyDiscrepanciesKey, KafkaSupplyDiscrepanciesValue> receivedRecord,
                                 Boolean dlqPublishEnabled, String dlqTopicName, Scheduler dlqSenderScheduler, KafkaSender<KafkaSupplyDiscrepanciesKey, KafkaSupplyDiscrepanciesValue> dlqSender,
                                 MeterRegistry meterRegistry) {
        UriComponents uriComponents;
        try {
            checkIfReceivedRecordIsValidCondition(receivedRecord);
            uriComponents = buildUriComponentsForSupDiscrepancies();
        } catch (Exception e) {
            handleError(receivedRecord, dlqPublishEnabled, dlqTopicName, dlqSenderScheduler, dlqSender, meterRegistry,
                    new NonRetryableIntegrationException(e.getMessage(), HttpStatus.BAD_REQUEST));
            return Mono.empty();
        }

        String requestBody;
        try {
            requestBody = objectMapper.writeValueAsString(transformInput(receivedRecord));
            log.debug("Request Body {} ",requestBody);
        } catch (JsonProcessingException e) {
            handleError(receivedRecord, dlqPublishEnabled, dlqTopicName, dlqSenderScheduler, dlqSender, meterRegistry,
                    new NonRetryableIntegrationException(e.getMessage(), HttpStatus.BAD_REQUEST));
            return Mono.empty();
        }

        try {
            jwtToken = generateJWTToken.generateToken();
        } catch (Exception e) {
            log.error("Error while generating token {} ", e);
            return Mono.error(new NonRetryableIntegrationException("Could not generate token"));
        }
        Timer timer = Timer.builder("supply.discrepancies.service.api.call")
                .tag("method", "makeWebRequestSupplyDiscrepanciesService").register(meterRegistry);

        // records time taken between Sample creation and registering the
        // stop() with the given Timer
        Timer.Sample sample = Timer.start(meterRegistry);
        Mono<Void> response = webClient
                .method(HttpMethod.POST)
                .uri(uriComponents.toUri())
                .headers(httpHeaders -> {
                    httpHeaders.set("Content-Type", "application/vnd.kafka.json.v2+json");
                    httpHeaders.set("Accept", "application/vnd.kafka.v2+json");
                    httpHeaders.set("Authorization", "Bearer " + jwtToken);
                })
                .body(requestBody != null ? BodyInserters.fromValue(requestBody) : BodyInserters.empty())
                .retrieve()
                .onStatus(httpStatus -> (httpStatus.is4xxClientError() && httpStatus.value() != HttpStatus.UNAUTHORIZED.value()),
                        clientResponse -> {
                            clientResponse.toEntity(getResponseClass()).subscribe(e -> {
                                getLogger().error("Response Body {}, Response Headers {}", e.getBody().toString(), e.getHeaders().toString());
                            });
                            getLogger().error("Error Throw: Non UNAUTHORIZED Error Status Code {}, URI {}, Token {}, Request Key {}, Request Value {}",
                                    clientResponse.rawStatusCode(), uriComponents.toUriString(), jwtToken,
                                    receivedRecord.key(), receivedRecord.value());
                            return Mono.error(new NonRetryableIntegrationException("Client error", clientResponse.statusCode()));
                        })
                .onStatus(httpStatus -> (httpStatus.is4xxClientError() && httpStatus.value() == HttpStatus.UNAUTHORIZED.value()),
                        clientResponse -> {
                            clientResponse.toEntity(getResponseClass()).subscribe(e -> {
                                getLogger().error("Response Body {}", e);
                            });
                            getLogger().error("Error Throw: Status Code {},  URI {}, Token {}, Request Key {}, Request Value {}",
                                    clientResponse.rawStatusCode(), uriComponents.toUriString(), jwtToken,
                                    receivedRecord.key(), receivedRecord.value());
                            try {
                                jwtToken = generateJWTToken.generateToken();
                            } catch (Exception e) {
                                return Mono.error(new NonRetryableIntegrationException("Could not generate token"));
                            }
                            getLogger().info("Refreshed token after token expired {}", jwtToken);
                            return Mono.error(new NonRetryableIntegrationException("Token Generation error", clientResponse.statusCode()));
                        })
                .onStatus(
                        httpStatus -> (httpStatus.is5xxServerError()),
                        clientResponse -> {
                            clientResponse.toEntity(getResponseClass()).subscribe(e -> {
                                getLogger().error("Response Body {}, Response Headers {}", e.getBody().toString(), e.getHeaders().toString());
                            });
                            getLogger().error("Error Throw: Status Code{}, URI{}, Token{}, Request Key {}, Request Value {}",
                                    clientResponse.rawStatusCode(), uriComponents.toUriString(), jwtToken,
                                    receivedRecord.key(), requestBody);
                            return Mono.error(new RetryableIntegrationException("Generic Server error", clientResponse.statusCode()));
                        })
                .bodyToMono(Void.class)
//                 .doOnError(throwable -> throwable.getMessage().equalsIgnoreCase("Token expired error"))
                .retryWhen(Retry.anyOf(RetryableIntegrationException.class)
                        .withApplicationContext(receivedRecord)
                        .exponentialBackoff(config.getInitialBackOff(), config.getMaxBackOff())
                        .retryMax(config.getRetryLimit())
                        .doOnRetry(objectRetryContext -> getLogger().info("retrying... {}", objectRetryContext)))
                .retryWhen(Retry.anyOf(ConnectException.class)
                        .withApplicationContext(receivedRecord)
                        .exponentialBackoff(config.getInitialBackOff(), config.getMaxBackOff())
                        .retryMax(config.getRetryLimit())
                        .doOnRetry(objectRetryContext -> getLogger().info("retrying... {}", objectRetryContext)))
                .flatMap(x -> {
                    getLogger().debug("Response Body {} URI {} for Request {}", x.toString(), uriComponents.toString(), requestBody);
                    return Mono.just(x);
                })
                .doOnError(exception -> {
                    if (exception.getMessage().equals("Token Generation error")) {
                        publish(receivedRecord,
                                dlqPublishEnabled, dlqTopicName, dlqSenderScheduler, dlqSender,
                                meterRegistry).subscribe();
                    } else {
                        handleError(receivedRecord, dlqPublishEnabled, dlqTopicName, dlqSenderScheduler, dlqSender, meterRegistry, exception);

                        if (exception instanceof BadRequestException || exception instanceof NonRetryableIntegrationException) {
                            consume();
                        } else {
                            waitForCircuitClose(yantriksKafkaRestHealthCheck);
                        }
                    }
                })
                .doOnTerminate(() -> {
                    getLogger().info("Terminating {}...", Thread.currentThread().getName());
                }).onErrorStop();
        sample.stop(timer);
        return response;


    }

    private SupplyDiscrepanciesPublishEntity transformInput(ReceiverRecord<KafkaSupplyDiscrepanciesKey, KafkaSupplyDiscrepanciesValue> receivedRecord) {
        log.debug("ReceivedRecord key: {}, value: {}  ", receivedRecord.key(), receivedRecord.value());
        SupplyDiscrepanciesPublishEntity supplyDiscrepanciesPublishEntity = new SupplyDiscrepanciesPublishEntity();
        KafkaSupplyDiscrepanciesValue kafkaSupplyDiscrepanciesValue = receivedRecord.value();
        supplyDiscrepanciesPublishEntity.setIsFullyQualifiedTopicName(false);
        supplyDiscrepanciesPublishEntity.setKey(supplyDiscrepancyMapperService.transformKey(kafkaSupplyDiscrepanciesValue));
        supplyDiscrepanciesPublishEntity.setOperation(OperationType.CREATE);
        supplyDiscrepanciesPublishEntity.setTopic(publishTopicName);
        supplyDiscrepanciesPublishEntity.setValue(supplyDiscrepancyMapperService.transformUpdatePayload(kafkaSupplyDiscrepanciesValue));
        log.info("supplyDiscrepanciesPublishEntity transorm {} ", supplyDiscrepanciesPublishEntity.toString());
        return supplyDiscrepanciesPublishEntity;
    }

    private UriComponents buildUriComponentsForSupDiscrepancies() {
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        return UriComponentsBuilder
                .fromHttpUrl(supDisRestUrl)
                .queryParams(queryParams)
                .build();
    }

    private void waitForCircuitClose(String url) {
        AtomicBoolean health = new AtomicBoolean(false);
        final Disposable subscribed = monitorApiServerHealth(url)
                .doOnNext(x -> {
                    if (x.getCode().contentEquals("UP")) {
                        health.set(true);
                    } else {
                        getLogger().warn("Remote Server is yet not reachable here ");
                    }
                })
                .subscribe();
        while (!health.get()) {
        }
        getLogger().info("Remote Server is reachable,disposing monitoring");
        subscribed.dispose();
        getLogger().info("Recreating Consumer...");
        consume();
    }

    private Flux<Status> monitorApiServerHealth(String url) {
        UriComponents uriComponents = UriComponentsBuilder
            .fromUriString(url).build();
        HealthDto fallback = new HealthDto();
        fallback.setStatus(Status.UNKNOWN);
        return Flux
            .interval(Duration.ofSeconds(circuitOpenInterval))
            .flatMap(i -> webClient
                .method(HttpMethod.GET)
                .uri(uriComponents.toUri())
                .headers(httpHeaders -> {
                    httpHeaders.set("Authorization", "Bearer " + jwtToken);
                })
                .retrieve()
                .bodyToMono(HealthDto.class)
                .onErrorReturn(fallback)
                .map(HealthDto::getStatus));
    }


    public void checkIfReceivedRecordIsValidCondition(ReceiverRecord<KafkaSupplyDiscrepanciesKey, KafkaSupplyDiscrepanciesValue> receivedRecord) {
        KafkaSupplyDiscrepanciesValue kafkaSupplyDiscrepanciesValue = receivedRecord.value();
//        log.info("testing debug {}", kafkaSupplyDiscrepanciesValue.getLocationId());
        if (StringUtils.isEmpty(kafkaSupplyDiscrepanciesValue.getLocationId())) {
            throw new BadRequestException("Location ID is null : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST);
        }

        if (StringUtils.isEmpty(receivedRecord.value().getProductId())) {
            throw new BadRequestException("Product ID is null : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST);
        }

        if (receivedRecord.value().getLocationType().equalsIgnoreCase("DC")) {
            //do nothing
        } else if (receivedRecord.value().getLocationType().equalsIgnoreCase("STORE")) {
            //do nothing
        } else {
            throw new BadRequestException("Location Type is not STORE/DC : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST);
        }

        String locationId = kafkaSupplyDiscrepanciesValue.getLocationId();
        String padLocationId = "000";
        String paddedLocationId = (padLocationId.substring(0, padLocationId.length() - locationId.length())) + locationId;

        if (kafkaSupplyDiscrepanciesValue.getLocationType().equalsIgnoreCase("STORE") && dcLocations.contains(paddedLocationId)) {
            throw new BadRequestException("Location Type STORE is associated with DC Location ID : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST);
        }
    }

    private String getConfluentHealthUrl() {
        return supDisRestUrl;
    }

    @Override
    public String getTopicName() {
        return topicName;
    }

    @Override
    public Logger getLogger() {
        return log;
    }

    @Override
    public Class<KafkaSupplyDiscrepanciesKey> getKeyClass() {
        return KafkaSupplyDiscrepanciesKey.class;
    }

    @Override
    public Class<KafkaSupplyDiscrepanciesValue> getRequestClass() {
        return KafkaSupplyDiscrepanciesValue.class;
    }

    @Override
    public String getAPIEndpoint() {
        return null;
    }


    @Override
    public Class<Void> getResponseClass() {
        return Void.class;
    }

}

