package com.yantriks.academy.streamer.schedulers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yantriks.academy.streamer.kafka.configuration.RestApiConfigurationProperties;
import com.yantriks.academy.streamer.kafka.exceptions.RetryableIntegrationException;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import java.net.ConnectException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import reactor.retry.Retry;

@Slf4j
@Configuration
@EnableScheduling
@Component
@Profile("algolia")
public class DcToStoreMappingScheduler{
    @Autowired
    private WebClient webClient;

    @Value("${api.server.dc-mapped-stores}")
    private String stsUrl;

    @Value("${api.server.retry.limit:0}")
    private int retryLimit;

    @Value("${api.server.retry.backoff.initial:0}")
    private Duration initialBackOff;

    @Value("${api.server.retry.backoff.max:0s}")
    private Duration maxBackOff;

    private RestApiConfigurationProperties config;

    private ConcurrentHashMap<String, List<String>> newDcToStoreMap = new ConcurrentHashMap<>();

    @Autowired
    private MeterRegistry meterRegistry;

    @Autowired
    private ObjectMapper objectMapper;
    /*
        initialize the scheduler
     */
    @PostConstruct
    public void init() {
        log.info("Dc To Store Mapping Scheduler Started...!");
        config = generateRestApiConfigurationProperties();
        getDcToStoreMapping();
    }

    /*
       makes a HTTP GET call to dcToStore url to fetch the storid mapped with given dc id
    */
    protected Mono<String> makeWebRequestDCMappedStoresService(HttpMethod httpMethod, UriComponents uriComponents,
                                                               Optional<String> updateTime) {
        return webClient
                .method(httpMethod)
                .uri(uriComponents.toUri())
                .retrieve()
                .onStatus(
                        httpStatus -> (httpStatus.is5xxServerError() || httpStatus.is4xxClientError()),
                        clientResponse -> Mono.error(new RetryableIntegrationException("Generic Server error", clientResponse.statusCode(),
                                uriComponents.getScheme() + "://" + uriComponents.getHost() + ":" + uriComponents.getPort())))
                .bodyToMono(String.class)
                .retryWhen(Retry.anyOf(RetryableIntegrationException.class)
                        .exponentialBackoff(config.getInitialBackOff(), config.getMaxBackOff())
                        .retryMax(config.getRetryLimit())
                        .doOnRetry(objectRetryContext -> log.debug("retrying... {}", objectRetryContext)))
                .retryWhen(Retry.anyOf(ConnectException.class)
                        .exponentialBackoff(config.getInitialBackOff(), config.getMaxBackOff())
                        .retryMax(config.getRetryLimit()).doOnRetry(objectRetryContext -> log.debug("retrying... {}", objectRetryContext)))
                .doOnNext(c -> log.trace("Response Entity Body : {}, For Record {} Token: {}, URI {}, UpdateTime {}",
                        c, uriComponents.toUriString(), updateTime.get()))
                .onErrorStop();
    }

    /*
       fetches the dcToStoreId mapping and stores in cache and refreshes every 20min(configuable[api.server.cron.expression])
    */
    /*Runs at every 30th minute "*\/30 * * * *" */
    @Scheduled(cron = "${api.server.cron.expression}")
    @Timed(value = "sts.rest.proxy.scheduler")
    public void getDcToStoreMapping(){
        log.info("Executing Dc to Store Mapping Scheduler...!");
        final Map<String, List<String>>[] res = new Map[]{new ConcurrentHashMap<>()};
        makeWebRequestDCMappedStoresService(
                 HttpMethod.GET,
                buildUriComponentsForSts(), Optional.of(LocalDateTime.now().toString()))
                .flatMap(response->{
                    try {
                        res[0] = objectMapper.readValue(response, ConcurrentHashMap.class);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    if(!CollectionUtils.isEmpty(res[0])){
                        newDcToStoreMap = (ConcurrentHashMap<String, List<String>>) res[0];
                    }
                    log.trace("dcToStoreMap value: {}", newDcToStoreMap);
                return  Mono.just(res[0]);
                }).subscribe();
    }


    public Map<String, List<String>> getCache(){
        return newDcToStoreMap;
    }
    /*
      builds uri components for dcToStore rest call
   */
    private UriComponents buildUriComponentsForSts() {
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
//        queryParams.put("debug", Arrays.asList(String.valueOf("aso")));
        return UriComponentsBuilder
                .fromHttpUrl(stsUrl)
                .queryParams(queryParams)
                .build();
    }
    /*
      generate rest api configurations
   */
    protected RestApiConfigurationProperties generateRestApiConfigurationProperties() {
        return RestApiConfigurationProperties.builder()
                .retryLimit(retryLimit)
                .initialBackOff(initialBackOff)
                .maxBackOff(maxBackOff)
                .build();
    }
}
