package com.yantriks.academy.streamer.service.mapper;

import com.yantriks.academy.dto.supply_discrepancies.*;
import com.yantriks.academy.dto.supply_discrepancies.key.KafkaSupplyDiscrepanciesKey;
import com.yantriks.academy.streamer.constants.SupplyDiscrepanciesConstants;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Profile("supply-discrepancies")
public class SupplyDiscrepancyMapperServiceImpl implements SupplyDiscrepancyMapperService{

    @Value("${supply-discrepancy.zoneId:America/Chicago}")
    private String zoneId;

    public  KafkaSupplyDiscrepanciesKey transformKey(KafkaSupplyDiscrepanciesValue kafkaSupplyDiscrepanciesValue){
        KafkaSupplyDiscrepanciesKey supplyDiscrepanciesKey = new KafkaSupplyDiscrepanciesKey();
        supplyDiscrepanciesKey.setOrgId(SupplyDiscrepanciesConstants.ORG_ID);
        supplyDiscrepanciesKey.setProductId(getPaddedProductId(kafkaSupplyDiscrepanciesValue.getProductId()));
        supplyDiscrepanciesKey.setUom(SupplyDiscrepanciesConstants.UOM);
        supplyDiscrepanciesKey.setLocationType(kafkaSupplyDiscrepanciesValue.getLocationType());
        supplyDiscrepanciesKey.setLocationId(getPaddedLocationId(kafkaSupplyDiscrepanciesValue.getLocationId()));
        return supplyDiscrepanciesKey;
    }

    public String getPaddedProductId(String productId){
        String padProductId = "000000000";
        return (padProductId.substring(0, padProductId.length() - productId.length())) + productId;
    }

    public String getPaddedLocationId(String locationId){
//        log.info("testing debug Service IMpl{}", locationId);
        String padLocationId = "000";
        return (padLocationId.substring(0, padLocationId.length() - locationId.length())) + locationId;
    }

    public SupplyType getZeroSupplyType(String supply){
        SupplyType supplyType = new SupplyType();
        supplyType.setSupplyType(supply);
        supplyType.setQuantity(SupplyDiscrepanciesConstants.ZERO_QUANTITY);
        return supplyType;
    }

    public SupplyType getSupplyType(String supply, Double qty){
        SupplyType supplyType = new SupplyType();
        supplyType.setSupplyType(supply);
        supplyType.setQuantity(qty);
        return supplyType;
    }

    public  SupplyDiscrepanciesValue transformUpdatePayload(KafkaSupplyDiscrepanciesValue kafkaSupplyDiscrepanciesValue){
        SupplyDiscrepanciesValue supplyDiscrepanciesValue = new SupplyDiscrepanciesValue();
        supplyDiscrepanciesValue.setOrgId(SupplyDiscrepanciesConstants.ORG_ID);
        supplyDiscrepanciesValue.setProductId(getPaddedProductId(kafkaSupplyDiscrepanciesValue.getProductId()));
        supplyDiscrepanciesValue.setUom(SupplyDiscrepanciesConstants.UOM);
        supplyDiscrepanciesValue.setEventType(SupplyDiscrepanciesConstants.EVENT_TYPE);
        supplyDiscrepanciesValue.setFeedType(SupplyDiscrepanciesConstants.ABSOLUTE);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        ZonedDateTime zonedDateTime = LocalDateTime.parse(kafkaSupplyDiscrepanciesValue.getExtractTimestamp(), formatter).atZone(ZoneId.of(getZoneId()));
        ZonedDateTime convertedDateTime = zonedDateTime.withZoneSameInstant(ZoneId.of("UTC"));
        log.debug("current dateTime :{}, changed to zone :{}",zonedDateTime,convertedDateTime);
        supplyDiscrepanciesValue.setUpdateTimeStamp(convertedDateTime.toOffsetDateTime().toString());
        supplyDiscrepanciesValue.setLocationType(kafkaSupplyDiscrepanciesValue.getLocationType());
        supplyDiscrepanciesValue.setLocationId(getPaddedLocationId(kafkaSupplyDiscrepanciesValue.getLocationId()));
        List<SupplyType> supplyTypeArrayList =new ArrayList<>();

        if(kafkaSupplyDiscrepanciesValue.getCustomerResv() != null){
            supplyTypeArrayList.add(
                    getSupplyType(SupplyDiscrepanciesConstants.CUSTOMER_RESV,
                            kafkaSupplyDiscrepanciesValue.getCustomerResv())
            );
        }else{
            supplyTypeArrayList.add(getZeroSupplyType(SupplyDiscrepanciesConstants.CUSTOMER_RESV));
        }

        if(kafkaSupplyDiscrepanciesValue.getInTransit()!=null){
            supplyTypeArrayList.add(
                    getSupplyType(SupplyDiscrepanciesConstants.IN_TRANSIT,
                            kafkaSupplyDiscrepanciesValue.getInTransit())
            );
        }else{
            supplyTypeArrayList.add(getZeroSupplyType(SupplyDiscrepanciesConstants.IN_TRANSIT));

        }

        if(kafkaSupplyDiscrepanciesValue.getTsfReserved()!=null){
            supplyTypeArrayList.add(
                    getSupplyType(SupplyDiscrepanciesConstants.TSF_RESERVED,
                            kafkaSupplyDiscrepanciesValue.getTsfReserved())
            );
        }else{
            supplyTypeArrayList.add(getZeroSupplyType(SupplyDiscrepanciesConstants.TSF_RESERVED));
        }

        if(kafkaSupplyDiscrepanciesValue.getTsfExpected()!=null){
            supplyTypeArrayList.add(
                    getSupplyType(SupplyDiscrepanciesConstants.TSF_EXPECTED,
                            kafkaSupplyDiscrepanciesValue.getTsfExpected())
            );
        }else{
            supplyTypeArrayList.add(getZeroSupplyType(SupplyDiscrepanciesConstants.TSF_EXPECTED));
        }

        if(kafkaSupplyDiscrepanciesValue.getRtv()!=null){
            supplyTypeArrayList.add(
                    getSupplyType(SupplyDiscrepanciesConstants.RTV,
                            kafkaSupplyDiscrepanciesValue.getRtv())
            );
        }else{
            supplyTypeArrayList.add(getZeroSupplyType(SupplyDiscrepanciesConstants.RTV));
        }

        if(kafkaSupplyDiscrepanciesValue.getNonSellable()!=null){
            supplyTypeArrayList.add(
                    getSupplyType(SupplyDiscrepanciesConstants.NON_SELLABLE,
                            kafkaSupplyDiscrepanciesValue.getNonSellable())
            );
        }else{
            supplyTypeArrayList.add(getZeroSupplyType(SupplyDiscrepanciesConstants.NON_SELLABLE));
        }

        if(kafkaSupplyDiscrepanciesValue.getDistro()!=null){
            supplyTypeArrayList.add(
                    getSupplyType(SupplyDiscrepanciesConstants.DISTRO,
                            kafkaSupplyDiscrepanciesValue.getDistro())
            );
        }else{
            supplyTypeArrayList.add(getZeroSupplyType(SupplyDiscrepanciesConstants.DISTRO));
        }

        if(kafkaSupplyDiscrepanciesValue.getSoh()!=null){
            supplyTypeArrayList.add(
                    getSupplyType(SupplyDiscrepanciesConstants.SOH,
                            kafkaSupplyDiscrepanciesValue.getSoh())
            );
        }else{
            supplyTypeArrayList.add(getZeroSupplyType(SupplyDiscrepanciesConstants.SOH));
        }

        SupplyTo supplyTo = new SupplyTo();
        supplyTo.setSupplyTypes(supplyTypeArrayList);
        supplyDiscrepanciesValue.setTo(supplyTo);

        Audit audit = new Audit();
        audit.setTransactionId(SupplyDiscrepanciesConstants.AUDIT_TRANSACTION_ID);
        audit.setTransactionReason(SupplyDiscrepanciesConstants.AUDIT_TRANSACTION_REASON);
        audit.setTransactionSystem(SupplyDiscrepanciesConstants.AUDIT_TRANSACTION_SYSTEM);
        audit.setTransactionType(SupplyDiscrepanciesConstants.AUDIT_TRANSACTION_TYPE);
        audit.setTransactionUser(SupplyDiscrepanciesConstants.AUDIT_TRANSACTION_USER);
        supplyDiscrepanciesValue.setAudit(audit);

        return supplyDiscrepanciesValue;
    }

    private String getZoneId() {
        try {
            zoneId = ZoneId.of(zoneId).toString();
        }catch (Exception e){
            zoneId = ZoneId.of("America/Chicago").toString();
        }
        return zoneId;
    }

}
