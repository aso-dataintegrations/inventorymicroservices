package com.yantriks.academy.streamer.consumer;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.yantriks.academy.dto.InventoryMap;
import com.yantriks.academy.dto.KafkaAtpPublishEntity;
import com.yantriks.academy.dto.key.KafkaProductKey;
import com.yantriks.academy.streamer.kafka.consumers.AbstractKafkaRestIntegration;
import com.yantriks.academy.streamer.kafka.exceptions.NonRetryableIntegrationException;
import com.yantriks.academy.streamer.service.algolia.AlgoliaService;
import com.yantriks.academy.streamer.service.mapper.FulfillmentTypeMapperService;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.kafka.receiver.ReceiverRecord;
import reactor.kafka.sender.KafkaSender;

@Slf4j
@Component
@Profile("algolia-network")
@EnableConfigurationProperties
public class NetworkAtpThresholdAlgoliaIntegration extends AbstractKafkaRestIntegration<KafkaProductKey, KafkaAtpPublishEntity, Object> {

  @Value("${streamer.aso-product-atp-threshold-pub}")
  private String topicName;
  @Value("${streamer.aso-product-atp-threshold-pub-ft}")
  private List<String> fulfillmentTypeList;
  @Autowired(required = false)
  AlgoliaService algoliaService;
  @Autowired
  ApplicationContext context;

  @Override
  public Logger getLogger() {
    return log;
  }

  @Override
  public Class<KafkaProductKey> getKeyClass() {
    return KafkaProductKey.class;
  }

  @Override
  public Class<KafkaAtpPublishEntity> getRequestClass() {
    return KafkaAtpPublishEntity.class;
  }

  @Override
  public String getAPIEndpoint() {
    return null;
  }

  @Override
  public Class<Object> getResponseClass() {
    return null;
  }

  @Override
  public String getTopicName() {
    return topicName;
  }

  @Autowired
  FulfillmentTypeMapperService fulfillmentTypeMapperService;

  /*
    processes the fetched consumer record and publish to algolia index
  */
  @Override
  protected Mono<Object> publish(ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> receivedRecord, Boolean dlqPublishEnabled,
      String dlqTopicName, Scheduler dlqSenderScheduler, KafkaSender<KafkaProductKey, KafkaAtpPublishEntity> dlqSender, MeterRegistry meterRegistry) {

    return Mono.defer(() -> {
      checkIfReceivedRecordIsValid(receivedRecord);
      return Mono.just(true);
    }).flatMap(val -> {
      checkForMandatoryAttributes(receivedRecord);
      return Mono.just(receivedRecord);
    }).zipWith(Mono.just(transformInput(receivedRecord))
    ).flatMap(tuple -> tuple.getT2().flatMap(inventoryMap -> {
      log.debug("InventoryMap details: {}", inventoryMap);
      algoliaService.saveIndex(inventoryMap);
      return tuple.getT2();
    }));

  }
  /*
    transforms the fetched consumer record into index object
  */
  private Mono<InventoryMap> transformInput(ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> receivedRecord) {
    log.debug("ReceivedRecord key: {}, value: {}  ", receivedRecord.key(), receivedRecord.value());
    KafkaProductKey key = receivedRecord.key();
    KafkaAtpPublishEntity kafkaAtpPublishEntity = receivedRecord.value();
    String productId = key.getProductId();
    return Flux.fromIterable(receivedRecord.value().getEntity().getAvailabilityByProducts()).reduce(new InventoryMap(), (inventoryMap, availabilityByProducts) -> {
      inventoryMap.setObjectID(productId);
      inventoryMap.putAll(fulfillmentTypeMapperService.processRecordByFulfillmentType(null, kafkaAtpPublishEntity.getUpdateTime(), null, inventoryMap, availabilityByProducts, fulfillmentTypeList));
      return inventoryMap;
    });
  }
  /*
     check mandatory attributes(productId, locationId, locationType)
  */
  protected void checkForMandatoryAttributes(ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> receivedRecord) {
    if (receivedRecord.key().getProductId() == null || receivedRecord.key().getProductId().length() == 0) {
      throw new NonRetryableIntegrationException("Mandatory Parameters missing :: ProductId is null or missing");
    }
  }

}
