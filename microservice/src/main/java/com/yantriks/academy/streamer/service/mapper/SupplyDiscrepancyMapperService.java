package com.yantriks.academy.streamer.service.mapper;

import com.yantriks.academy.dto.supply_discrepancies.KafkaSupplyDiscrepanciesValue;
import com.yantriks.academy.dto.supply_discrepancies.SupplyDiscrepanciesValue;
import com.yantriks.academy.dto.supply_discrepancies.key.KafkaSupplyDiscrepanciesKey;

public interface SupplyDiscrepancyMapperService {

    KafkaSupplyDiscrepanciesKey transformKey(KafkaSupplyDiscrepanciesValue kafkaSupplyDiscrepanciesValue);
    SupplyDiscrepanciesValue transformUpdatePayload(KafkaSupplyDiscrepanciesValue kafkaSupplyDiscrepanciesValue);

}
