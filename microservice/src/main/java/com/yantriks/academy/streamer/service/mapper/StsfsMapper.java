package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_AVAILABILITY_STATUS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_INV_UPDTAE_FLAG;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_OOSTIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH_QUANTITY;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_AVAILABILITY_STATUS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_INV_UPDTAE_FLAG;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_OOSTIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS_QUANTITY;

import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import com.yantriks.academy.dto.KafkaAtpPublishBySegment;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile("algolia")
@Qualifier("STSFS")
@EnableConfigurationProperties
public class StsfsMapper extends Mapper {
  /*
 maps consumer records for fulfillmentType STH
  */
  @Override
  public Map<String, Object> mapRecords(String locationId, LocalDateTime updateTime, KafkaAtpPublishByFT availabilityByFulfillmentTypes) {
    Map<String, Object> inventoryMap = new HashMap<>();
    for (KafkaAtpPublishBySegment atpPublishBySegment : availabilityByFulfillmentTypes.getAvailabilityDetails()) {
      Double atp = atpPublishBySegment.getAtp();
      Map<String, Object> inventoryIndexMap = new HashMap<>();

      if (atp<=0) {
        inventoryIndexMap.put(STSFS_OOSTIME, updateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli());
      } else {
        inventoryIndexMap.put(STSFS_OOSTIME, 'N');
      }

      inventoryIndexMap.put(STSFS_INDEX_TIME, updateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli());
      inventoryIndexMap.put(STSFS_INV_UPDTAE_FLAG, atp > 0 ? 'Y' : 'N');
      inventoryIndexMap.put(STSFS_AVAILABILITY_STATUS,  getValidAtpStatus(atpPublishBySegment.getAtpStatus()));
      inventoryIndexMap.put(STSFS_QUANTITY, atp);
      inventoryMap.putAll(inventoryIndexMap);

    }
    return inventoryMap;
  }
}
