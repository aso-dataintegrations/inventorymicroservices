package com.yantriks.academy.streamer.consumer;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.OBJECT_ID;

import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.common.header.Header;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2;
import org.springframework.stereotype.Component;

import com.algolia.search.SearchIndex;
import com.yantriks.academy.dto.InventoryMap;
import com.yantriks.academy.dto.KafkaAtpPublishByProduct;
import com.yantriks.academy.dto.KafkaAtpPublishEntity;
import com.yantriks.academy.dto.key.KafkaProductKey;
import com.yantriks.academy.streamer.kafka.constants.PublishOperation;
import com.yantriks.academy.streamer.kafka.consumers.AbstractKafkaBatchRestIntegration;
import com.yantriks.academy.streamer.kafka.exceptions.NonRetryableIntegrationException;
import com.yantriks.academy.streamer.service.algolia.AlgoliaService;
import com.yantriks.academy.streamer.service.mapper.FulfillmentTypeMapperService;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.kafka.receiver.ReceiverRecord;
import reactor.kafka.sender.KafkaSender;

@Slf4j
@Component
@Profile({"algolia-network-atp-batch","algolia-network-fullindex"})
@EnableConfigurationProperties
public class NetworkAtpBatchAlgoliaIntegration extends AbstractKafkaBatchRestIntegration<KafkaProductKey, KafkaAtpPublishEntity> {

  @Autowired
  @Qualifier("inventoryIndex")
  SearchIndex<InventoryMap> inventoryIndex;
  @Value("${streamer.aso-product-atp-pub}")
  private String topicName;
  @Value("${streamer.aso-product-atp-pub-ft}")
  private List<String> fulfillmentTypeList;
  @Autowired(required = false)
  AlgoliaService algoliaService;
  @Autowired
  ApplicationContext context;

  @Override
  public Logger getLogger() {
    return log;
  }

  @Override
  public Class<KafkaProductKey> getKeyClass() {
    return KafkaProductKey.class;
  }

  @Override
  public Class<KafkaAtpPublishEntity> getRequestClass() {
    return KafkaAtpPublishEntity.class;
  }

  @Override
  public String getTopicName() {
    return topicName;
  }

  @Autowired
  FulfillmentTypeMapperService fulfillmentTypeMapperService;
  /*
        processes the fetched consumer records and publish to algolia index
   */
  @Override
  protected Flux<List<ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>>> publish(List<ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>> receivedRecords,
      PublishOperation publishOperation, Boolean dlqPublishEnabled, String dlqTopicName, Scheduler dlqSenderScheduler, KafkaSender<KafkaProductKey, KafkaAtpPublishEntity> dlqSender, MeterRegistry meterRegistry) {
    List<InventoryMap> inventoryMapList = new ArrayList<>();
    return Flux.fromIterable(receivedRecords)
        .reduce(new ArrayList<ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>>(), (reducedReceivedRecordList,
            receiverRecord) -> {
          if (checkIfReceivedRecordIsValid(receiverRecord) && checkForMandatoryAttributes(receiverRecord)) {
        	  removeDuplicateReceiverRecordfromReceiverRecordList(reducedReceivedRecordList, receiverRecord);
          }
          return reducedReceivedRecordList;
        })
        .flatMapMany(Flux::fromIterable)
        .flatMap(receivedRecord -> {
          InventoryMap map = transformInput(receivedRecord);
          if(!map.isEmpty()) {
            inventoryMapList.add(map);
          }
          return Flux.just(receivedRecords);
        }).doOnComplete(() -> algoliaService.saveIndex(inventoryMapList));

  }
  /*
          transforms the fetched consumer record into index object
     */
  private InventoryMap transformInput(ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> receivedRecord) {
    InventoryMap inventoryMap = new InventoryMap();
      log.debug("ReceivedRecord key: {}, value: {}  ", receivedRecord.key(), receivedRecord.value());

      KafkaProductKey key = receivedRecord.key();
      KafkaAtpPublishEntity kafkaLocationInventoryAtpPublishEntity = receivedRecord.value();
      String productId = key.getProductId();
      for(KafkaAtpPublishByProduct availabilityByProducts : receivedRecord.value().getEntity().getAvailabilityByProducts()) {
        inventoryMap.putAll(fulfillmentTypeMapperService.processRecordByFulfillmentType(null, kafkaLocationInventoryAtpPublishEntity.getUpdateTime(), null, inventoryMap, availabilityByProducts, fulfillmentTypeList));
      }
    if(!inventoryMap.isEmpty()){
      inventoryMap.put(OBJECT_ID, productId);
    }
      return inventoryMap;
  }
  /*
    check if received record is valid
  */
  protected boolean checkIfReceivedRecordIsValid(ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> receivedRecord) {
    Header keyErrorHeader = receivedRecord.headers().lastHeader(ErrorHandlingDeserializer2.KEY_DESERIALIZER_EXCEPTION_HEADER);
    Header valueErrorHeader = receivedRecord.headers().lastHeader(ErrorHandlingDeserializer2.VALUE_DESERIALIZER_EXCEPTION_HEADER);

    if ((keyErrorHeader != null && keyErrorHeader.value() != null) || (valueErrorHeader != null && valueErrorHeader.value() != null)) {
      receivedRecord.key().setDlqPublish(true);
      receivedRecord.key().setThrowable(new NonRetryableIntegrationException("Failed to deserialize key/value for topic : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST));
      return false;
    }
    if (receivedRecord.key() == null) {
      receivedRecord.key().setDlqPublish(true);
      receivedRecord.key().setThrowable(new NonRetryableIntegrationException("key is invalid/null : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST));
      return false;

    }
    if (receivedRecord.value() == null) {
      receivedRecord.key().setDlqPublish(true);
      receivedRecord.key().setThrowable(new NonRetryableIntegrationException("value is invalid/null : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST));
      return false;
    }
    return true;
  }
  /*
       check mandatory attributes(productId, locationId, locationType)
    */
  protected boolean checkForMandatoryAttributes(ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> receivedRecord) {
    if (receivedRecord.key().getProductId() == null || receivedRecord.key().getProductId().length() == 0) {
      receivedRecord.key().setDlqPublish(true);
      receivedRecord.key().setThrowable(new NonRetryableIntegrationException("Mandatory Parameters missing :: Product Id is null or missing"));
      return false;
    }
    return true;
  }
  
  public void removeDuplicateReceiverRecordfromReceiverRecordList(List<ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>> reducedReceivedRecordList, ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity>receiverRecord) {
	  ReceiverRecord<KafkaProductKey, KafkaAtpPublishEntity> duplicateRecord = reducedReceivedRecordList.stream().filter(r -> r.key().equals(receiverRecord.key())
	            && !r.value().getUpdateTime().isAfter(receiverRecord.value().getUpdateTime())).findFirst().orElse(null);
	            if (duplicateRecord != null) {
	            	log.debug("Duplicate Record : " + duplicateRecord);
	              reducedReceivedRecordList.remove(duplicateRecord);
	            }
	            reducedReceivedRecordList.add(receiverRecord);
  }

}
