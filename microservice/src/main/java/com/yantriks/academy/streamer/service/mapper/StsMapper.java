package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.AVAILABILITY_STATUS_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS_ID_SUFFIX;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS_INDEX_TIME;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS_PREFIX;

import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import com.yantriks.academy.dto.KafkaAtpPublishBySegment;
import com.yantriks.academy.streamer.kafka.exceptions.NonRetryableIntegrationException;
import com.yantriks.academy.streamer.schedulers.DcToStoreMappingScheduler;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile("algolia")
@Qualifier("StsMapper")
@EnableConfigurationProperties
public class StsMapper extends Mapper{

    @Autowired
    DcToStoreMappingScheduler dcToStoreMappingScheduler;
    /*
    maps consumer records for fulfillmentType STS
  */
    @Override
    public Map<String, Object> mapRecords(String locationId, LocalDateTime updateTime, KafkaAtpPublishByFT availabilityByFulfillmentTypes) {
        Map<String, Object> inventoryMap = new HashMap<>();
        locationId = locationId.replaceFirst("^0+(?!$)", "");
        List<String> stores = dcToStoreMappingScheduler.getCache().get(locationId);
        if(stores == null || stores.isEmpty()){
            String errorMessage = "No stores mapped to DC id: "+ locationId;
            throw new NonRetryableIntegrationException(errorMessage);
        }
        inventoryMap.put(STS_INDEX_TIME, Calendar.getInstance().getTimeInMillis());
        for (KafkaAtpPublishBySegment atpPublishBySegment : availabilityByFulfillmentTypes.getAvailabilityDetails()) {
            Double atp = atpPublishBySegment.getAtp();
            String atpStatus = getValidAtpStatus(atpPublishBySegment.getAtpStatus());
            for (String store : stores) {
                store= StringUtils.leftPad(store,3,"0");
                inventoryMap.put(STS_PREFIX + store, atp);
                inventoryMap.put(STS_PREFIX + store + STS_ID_SUFFIX, locationId);
                inventoryMap.put(STS_PREFIX + store + AVAILABILITY_STATUS_SUFFIX, atpStatus);
            }
        }
        return inventoryMap;
    }
}
