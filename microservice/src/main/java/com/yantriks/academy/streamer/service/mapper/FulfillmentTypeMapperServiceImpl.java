package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.DC;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LSI;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.PICK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STH;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STORE;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STS;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.STSFS;

import com.yantriks.academy.dto.InventoryMap;
import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Profile("algolia")
public class FulfillmentTypeMapperServiceImpl implements FulfillmentTypeMapperService {

    @Autowired
    LsiMapper lsiMapper;

    @Autowired
    StsMapper stsMapper;

    @Autowired
    ShipMapper shipMapper;

    @Autowired
    StsfsMapper stsfsMapper;

    @Autowired
    PickMapper pickMapper;
    /*
    processes record based on fulfillment type
     */
    public Map<String, Object> processRecordByFulfillmentType(String locationType, LocalDateTime updateTime, String locationId, InventoryMap inventoryMap, com.yantriks.academy.dto.KafkaAtpPublishByProduct availabilityByProducts, List<String> fulfillmentTypeList) {
            for (KafkaAtpPublishByFT availabilityByFulfillmentTypes : availabilityByProducts.getAvailabilityByFulfillmentTypes()) {
                if (isEligibleFT(availabilityByFulfillmentTypes.getFulfillmentType(), locationType, fulfillmentTypeList)) {
                    inventoryMap.putAll(getInventoryMapByFulfillmentType(updateTime, locationId, availabilityByFulfillmentTypes));
                } else {
                    log.debug("Skipped algolia inventory index update for {} in case of fulfillment type : {}", locationType==null?"Network":locationType,
                        availabilityByFulfillmentTypes.getFulfillmentType());
                }
            }
        return inventoryMap;
    }
    /*
    creates inventoryMap based on fulfillment type
     */
    private Map<String, Object> getInventoryMapByFulfillmentType(LocalDateTime updateTime, String locationId, KafkaAtpPublishByFT availabilityByFulfillmentTypes) {
        Map<String, Object> inventoryMap = new HashMap<>();
        switch (availabilityByFulfillmentTypes.getFulfillmentType().toUpperCase()){
            case PICK:
                inventoryMap = pickMapper.mapRecords(locationId,
                        updateTime, availabilityByFulfillmentTypes);
                break;
            case LSI:
                inventoryMap = lsiMapper.mapRecords(locationId,
                        updateTime, availabilityByFulfillmentTypes);
                break;
            case STH:
                inventoryMap = shipMapper.mapRecords(locationId,
                        updateTime, availabilityByFulfillmentTypes);
                break;
            case STSFS:
                inventoryMap = stsfsMapper.mapRecords(locationId,
                    updateTime, availabilityByFulfillmentTypes);
                break;
            case STS:
                inventoryMap = stsMapper.mapRecords(locationId,
                        updateTime, availabilityByFulfillmentTypes);
                break;
        }
        return inventoryMap;
    }
    /*
      checks whether the fulfillment is eligible for this topic
   */
    private boolean isEligibleFT(String fulfillmentType, String locationType, List<String> fulfillmentTypeList) {
        if (!StringUtils.isEmpty(locationType) && locationType.equalsIgnoreCase(STORE) && !fulfillmentType.equalsIgnoreCase(STS) && fulfillmentTypeList.contains(fulfillmentType)) {
            return true;
        } else if (!StringUtils.isEmpty(locationType) && locationType.equalsIgnoreCase(DC) && fulfillmentType.equalsIgnoreCase(STS) && fulfillmentTypeList.contains(fulfillmentType)){
            return true;
        }else if(StringUtils.isEmpty(locationType)){
            return fulfillmentTypeList.contains(fulfillmentType);
        }else {
            return false;
        }
    }
}
