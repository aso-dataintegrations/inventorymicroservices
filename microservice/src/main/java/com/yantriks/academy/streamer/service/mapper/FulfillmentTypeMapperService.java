package com.yantriks.academy.streamer.service.mapper;

import com.yantriks.academy.dto.InventoryMap;
import com.yantriks.academy.dto.KafkaAtpPublishByProduct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;


public interface FulfillmentTypeMapperService {
    // processes record by fulfillment type
    Map<String, Object> processRecordByFulfillmentType(String locationType, LocalDateTime updateTime, String locationId, InventoryMap inventoryMap,
        KafkaAtpPublishByProduct availabilityByProducts, List<String> fulfillmentTypeList);
}
