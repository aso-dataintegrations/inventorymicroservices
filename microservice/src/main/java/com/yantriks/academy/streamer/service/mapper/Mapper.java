package com.yantriks.academy.streamer.service.mapper;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.AVAILABLE;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.IN_STOCK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LIMITEDSTOCK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.LIMITED_STOCK;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.NOTAVAILABLE;
import static com.yantriks.academy.streamer.constants.AlgoliaConstants.OOS;

import com.yantriks.academy.dto.KafkaAtpPublishByFT;
import java.time.LocalDateTime;
import java.util.Map;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("algolia")
@EnableConfigurationProperties
public abstract class Mapper {
  /*
  maps consumer records based on fulfillmentType
   */
  abstract Map<String,Object> mapRecords(String locationId, LocalDateTime updateTime, KafkaAtpPublishByFT kafkaAtpPublishByFT);

  String getValidAtpStatus(String atpPulishATpStatus){
    switch (atpPulishATpStatus){
      case OOS:
        return NOTAVAILABLE;
      case IN_STOCK:
        return AVAILABLE;
      case LIMITED_STOCK:
        return LIMITEDSTOCK;
      default:
        return atpPulishATpStatus;
    }
  }
}
