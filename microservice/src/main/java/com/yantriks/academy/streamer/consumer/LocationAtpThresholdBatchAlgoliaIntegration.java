package com.yantriks.academy.streamer.consumer;

import static com.yantriks.academy.streamer.constants.AlgoliaConstants.OBJECT_ID;

import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.common.header.Header;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2;
import org.springframework.stereotype.Component;

import com.yantriks.academy.dto.InventoryMap;
import com.yantriks.academy.dto.KafkaAtpPublishByProduct;
import com.yantriks.academy.dto.KafkaLocationInventoryAtpPublishEntity;
import com.yantriks.academy.dto.key.KafkaLocationAtpPublishKey;
import com.yantriks.academy.streamer.kafka.constants.PublishOperation;
import com.yantriks.academy.streamer.kafka.consumers.AbstractKafkaBatchRestIntegration;
import com.yantriks.academy.streamer.kafka.exceptions.NonRetryableIntegrationException;
import com.yantriks.academy.streamer.service.algolia.AlgoliaService;
import com.yantriks.academy.streamer.service.mapper.FulfillmentTypeMapperService;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.kafka.receiver.ReceiverRecord;
import reactor.kafka.sender.KafkaSender;

@Slf4j
@Component
@Profile("algolia-location-batch")
@EnableConfigurationProperties
public class LocationAtpThresholdBatchAlgoliaIntegration extends AbstractKafkaBatchRestIntegration<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> {

	@Autowired
	ApplicationContext context;
	@Autowired(required = false)
	AlgoliaService algoliaService;
	@Autowired
	FulfillmentTypeMapperService fulfillmentTypeMapperService;
	@Value("${streamer.aso-product-location-atp-threshold-pub}")
	private String topicName;
	@Value("${streamer.aso-product-location-atp-threshold-pub-ft}")
	private List<String> fulfillmentTypeList;

	@Override
	public Logger getLogger() {
		return log;
	}

	@Override
	public Class<KafkaLocationAtpPublishKey> getKeyClass() {
		return KafkaLocationAtpPublishKey.class;
	}

	@Override
	public Class<KafkaLocationInventoryAtpPublishEntity> getRequestClass() {
		return KafkaLocationInventoryAtpPublishEntity.class;
	}

	@Override
	public String getTopicName() {
		return topicName;
	}

	/*
      processes the fetched consumer records and publish to algolia index
	 */
	protected Flux<List<ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity>>> publish(
			List<ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity>> receivedRecords, PublishOperation publishOperation,
			Boolean dlqPublishEnabled, String dlqTopicName, Scheduler dlqSenderScheduler, KafkaSender<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> dlqSender,
			MeterRegistry meterRegistry) {

		List<InventoryMap> inventoryMapList = new ArrayList<>();
		return Flux.fromIterable(receivedRecords)
				.reduce(new ArrayList<ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity>>(), (reducedReceivedRecordList,
						receiverRecord) -> {
							if (checkIfReceivedRecordIsValid(receiverRecord) && checkForMandatoryAttributes(receiverRecord)) {
								log.debug("ReceivedRecord key: {}, value: {}  ", receiverRecord.key(), receiverRecord.value());
								removeDuplicateReceiverRecordfromReceiverRecordList(reducedReceivedRecordList, receiverRecord);
							}
							return reducedReceivedRecordList;
						})
				.flatMapMany(Flux::fromIterable)
				.flatMap(receivedRecord -> {
					InventoryMap map = transformInput(receivedRecord);
					if(!map.isEmpty()) {
						inventoryMapList.add(map);
					}
					return Flux.just(receivedRecords);
				}).doOnComplete(() -> algoliaService.saveIndex(inventoryMapList));
	}

	/*
         transforms the fetched consumer record into index object
	 */
	private InventoryMap transformInput(ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> receivedRecord) {
		log.debug("ReceivedRecord key: {}, value: {}  ", receivedRecord.key(), receivedRecord.value());
		InventoryMap inventoryMap = new InventoryMap();
		KafkaLocationAtpPublishKey key = receivedRecord.key();
		KafkaLocationInventoryAtpPublishEntity kafkaLocationInventoryAtpPublishEntity = receivedRecord.value();
		String productId = key.getProductId();
		String locationId = key.getLocationId();
		String locationType = key.getLocationType();

		for (KafkaAtpPublishByProduct availabilityByProducts : receivedRecord.value().getEntity().getAvailabilityByProducts()) {
			inventoryMap.putAll(fulfillmentTypeMapperService
					.processRecordByFulfillmentType(locationType, kafkaLocationInventoryAtpPublishEntity.getUpdateTime(), locationId, inventoryMap, availabilityByProducts,
							fulfillmentTypeList));
		}
		if(!inventoryMap.isEmpty()){
			inventoryMap.put(OBJECT_ID, productId);
		}
		return inventoryMap;
	}

	/*
     check if received record is valid
	 */
	protected boolean checkIfReceivedRecordIsValid(ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> receivedRecord) {
		Header keyErrorHeader = receivedRecord.headers().lastHeader(ErrorHandlingDeserializer2.KEY_DESERIALIZER_EXCEPTION_HEADER);
		Header valueErrorHeader = receivedRecord.headers().lastHeader(ErrorHandlingDeserializer2.VALUE_DESERIALIZER_EXCEPTION_HEADER);

		if ((keyErrorHeader != null && keyErrorHeader.value() != null) || (valueErrorHeader != null && valueErrorHeader.value() != null)) {
			receivedRecord.key().setDlqPublish(true);
			receivedRecord.key().setThrowable(new NonRetryableIntegrationException("Failed to deserialize key/value for topic : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST));
			return false;
		}
		if (receivedRecord.key() == null) {
			receivedRecord.key().setDlqPublish(true);
			receivedRecord.key().setThrowable(new NonRetryableIntegrationException("key is invalid/null : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST));
			return false;

		}
		if (receivedRecord.value() == null) {
			receivedRecord.key().setDlqPublish(true);
			receivedRecord.key().setThrowable(new NonRetryableIntegrationException("value is invalid/null : " + receivedRecord.topic(), HttpStatus.BAD_REQUEST));
			return false;
		}
		return true;
	}

	/*
     check mandatory attributes(productId, locationId, locationType)
	 */
	private boolean checkForMandatoryAttributes(ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> receivedRecord) {
		if (receivedRecord.key().getLocationId() == null || receivedRecord.key().getLocationId().length() == 0) {
			receivedRecord.key().setDlqPublish(true);
			receivedRecord.key().setThrowable(new NonRetryableIntegrationException("Mandatory Parameters missing :: Location Id is null or missing"));
			return false;
		}
		if (receivedRecord.key().getProductId() == null || receivedRecord.key().getProductId().length() == 0) {
			receivedRecord.key().setDlqPublish(true);
			receivedRecord.key().setThrowable(new NonRetryableIntegrationException("Mandatory Parameters missing :: Product Id is null or missing"));
			return false;
		}
		if (receivedRecord.key().getLocationType() == null || receivedRecord.key().getLocationType().length() == 0) {
			receivedRecord.key().setDlqPublish(true);
			receivedRecord.key().setThrowable(new NonRetryableIntegrationException("Mandatory Parameters missing :: Location Type is null or missing"));
			return false;
		}
		return true;
	}

	public void removeDuplicateReceiverRecordfromReceiverRecordList(List<ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity>> reducedReceivedRecordList, ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity>receiverRecord) {
		ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> duplicateRecord =
				reducedReceivedRecordList.stream().filter(r -> r.key().equals(receiverRecord.key())
						&& !r.value().getUpdateTime().isAfter(receiverRecord.value().getUpdateTime())).findFirst().orElse(null);
		if (duplicateRecord != null) {
			log.debug("Duplicate Record : " + duplicateRecord);
			reducedReceivedRecordList.remove(duplicateRecord);
		}
		reducedReceivedRecordList.add(receiverRecord);
	}
}

