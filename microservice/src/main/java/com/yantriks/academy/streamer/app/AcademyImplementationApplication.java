package com.yantriks.academy.streamer.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@Slf4j
@SpringBootApplication
@ComponentScan({"com.yantriks.academy"})
@EnableConfigurationProperties
@EnableScheduling
public class AcademyImplementationApplication {
    public static void main(String[] args) {
        SpringApplication.run(AcademyImplementationApplication.class, args);
    }
}
