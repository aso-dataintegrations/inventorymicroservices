package com.yantriks.academy.streamer.controller;

import com.yantriks.academy.streamer.schedulers.DcToStoreMappingScheduler;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(path = "/api")
@Profile("algolia")
public class DcToStoreMappingController {
    @Autowired
    DcToStoreMappingScheduler dcToStoreMappingScheduler;
    //fetches caches data for dcTostoreId mapping
    @GetMapping(path="/dcstoremapping/cache")
    public Map<String, List<String>> getResponse() {
      return dcToStoreMappingScheduler.getCache();
    }
}
