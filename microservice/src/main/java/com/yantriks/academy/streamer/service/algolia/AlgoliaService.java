package com.yantriks.academy.streamer.service.algolia;

import com.algolia.search.SearchIndex;
import com.algolia.search.exceptions.AlgoliaRuntimeException;
import com.yantriks.academy.dto.InventoryMap;
import io.micrometer.core.annotation.Timed;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile("algolia")
@EnableConfigurationProperties

public class AlgoliaService {

  @Autowired
  @Qualifier("inventoryIndex")
  SearchIndex<InventoryMap> inventoryIndex;

  /*
      incremental updates by using algolia api partialUpdateObject for inventoryMap
   */
  @Timed("algolia.service.call.incremental")
  public void saveIndex(InventoryMap inventoryMap) {
    if (inventoryMap.size() > 1) {
      try {
        log.debug("Inventory index details: {}", inventoryIndex.getUrlEncodedIndexName());
        inventoryIndex.partialUpdateObjectAsync(inventoryMap, true);
      } catch (Exception e) {
        log.debug("Exception occurred while updating algolia index:{}", e.getMessage());
        throw new AlgoliaRuntimeException(e);
      }
      log.info("InventoryMap details[{}]: {}", inventoryMap.getObjectID(), inventoryMap.entrySet().toString());
    }
  }

  /*
      batch updates by using algolia api partialUpdateObjects for list of inventoryMap objects
  */
  @Timed("algolia.service.call.batch")
  public void saveIndex(List<InventoryMap> inventoryMapList) {
    if (inventoryMapList != null && !inventoryMapList.isEmpty()) {
      try {
        log.debug("Inventory index details: {}", inventoryIndex.getUrlEncodedIndexName());
        inventoryIndex.partialUpdateObjectsAsync(inventoryMapList, true);
      } catch (Exception e) {
        log.debug("Exception occurred while updating algolia index:{}", e.getMessage());
        throw new AlgoliaRuntimeException(e);
      }
      log.debug("InventoryMap List details:");
      inventoryMapList.stream().forEach(m -> log.info(m.entrySet().toString()));
    }
  }

}
