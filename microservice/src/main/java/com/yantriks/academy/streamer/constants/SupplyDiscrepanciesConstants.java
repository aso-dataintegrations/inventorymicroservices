package com.yantriks.academy.streamer.constants;

public class SupplyDiscrepanciesConstants {
    SupplyDiscrepanciesConstants() {throw new IllegalStateException("Constant class");}

    public static final String AUDIT_TRANSACTION_ID = "DEFAULT";
    public static final String AUDIT_TRANSACTION_REASON = "SKULOC";
    public static final String AUDIT_TRANSACTION_SYSTEM = "Inventory Service Consumer";
    public static final String AUDIT_TRANSACTION_TYPE = "INVENTORY_DISCREPANCY";
    public static final String AUDIT_TRANSACTION_USER = "INVS";
    public static final String ABSOLUTE = "ABSOLUTE";
    public static final String EVENT_TYPE = "SUPPLY_UPDATE";
    public static final String ORG_ID = "ACADEMY";
    public static final Double ZERO_QUANTITY = 0.0;
    public static final String UOM = "EACH";

    public static final String SOH = "SOH";
    public static final String TSF_RESERVED = "TSF_RESERVED";
    public static final String RTV = "RTV";
    public static final String NON_SELLABLE = "NON_SELLABLE";
    public static final String IN_TRANSIT = "IN_TRANSIT";
    public static final String RESERVED = "RESERVED";
    public static final String CUSTOMER_RESV = "CUSTOMER_RESV";
    public static final String DISTRO = "DISTRO";
    public static final String TSF_EXPECTED = "TSF_EXPECTED";
}
