package com.yantriks.academy.streamer.config;

import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.config.MeterFilter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricsConfig {
    /*
     timed aspect
    */
    @Bean
    public TimedAspect timedAspect(MeterRegistry registry) {
        return new TimedAspect(registry);
    }
    /*
     hostname
    */
    @Bean
    public String hostname() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostName();
    }

    @Bean
    public MeterFilter renameEmptyTags() {
        return MeterFilter.replaceTagValues("exception", exception -> exception.isEmpty() ? "NONE" : exception);
    }

    @Bean
    public MeterRegistryCustomizer<MeterRegistry> registryCustomizer(String hostname) {
        return registry -> registry.config().commonTags("host", hostname);
    }


}

