package com.yantriks.academy.streamer.consumer;

import com.yantriks.academy.dto.InventoryMap;
import com.yantriks.academy.dto.KafkaLocationInventoryAtpPublishEntity;
import com.yantriks.academy.dto.key.KafkaLocationAtpPublishKey;
import com.yantriks.academy.streamer.kafka.consumers.AbstractKafkaRestIntegration;
import com.yantriks.academy.streamer.kafka.exceptions.BadRequestException;
import com.yantriks.academy.streamer.kafka.exceptions.NonRetryableIntegrationException;
import com.yantriks.academy.streamer.service.algolia.AlgoliaService;
import com.yantriks.academy.streamer.service.mapper.FulfillmentTypeMapperService;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.kafka.receiver.ReceiverRecord;
import reactor.kafka.sender.KafkaSender;

@Slf4j
@Component
@Profile("algolia-location")
@EnableConfigurationProperties
public class LocationAtpThresholdAlgoliaIntegration extends AbstractKafkaRestIntegration<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity, Object> {

    @Autowired(required = false)
    AlgoliaService algoliaService;
    @Value("${streamer.aso-product-location-atp-threshold-pub}")
    private String topicName;
    @Value("${streamer.aso-product-location-atp-threshold-pub-ft}")
    private List<String> fulfillmentTypeList;

    @Override
    public Logger getLogger() {
        return log;
    }

    @Override
    public Class<KafkaLocationAtpPublishKey> getKeyClass() {
        return KafkaLocationAtpPublishKey.class;
    }

    @Override
    public Class<KafkaLocationInventoryAtpPublishEntity> getRequestClass() {
        return KafkaLocationInventoryAtpPublishEntity.class;
    }

    @Override
    public String getAPIEndpoint() {
        return null;
    }

    @Override
    public Class<Object> getResponseClass() {
        return null;
    }

    @Override
    public String getTopicName() {
        return topicName;
    }

    @Autowired
    FulfillmentTypeMapperService fulfillmentTypeMapperService;

    /*
      processes the fetched consumer record and publish to algolia index
    */
    @Override
    protected Mono<Object> publish(ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> receivedRecord,
                                   Boolean dlqPublishEnabled, String dlqTopicName, Scheduler dlqSenderScheduler, KafkaSender<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> dlqSender,
                                   MeterRegistry meterRegistry) {
        return Mono.defer(() -> {
            checkIfReceivedRecordIsValid(receivedRecord);
            return Mono.just(true);
        }).flatMap(val -> {
            checkForMandatoryAttributes(receivedRecord);
            return Mono.just(receivedRecord);
        }).zipWith(Mono.just(transformInput(receivedRecord))
        ).flatMap(tuple -> tuple.getT2().flatMap(inventoryMap -> {
            log.debug("InventoryMap details: {}", inventoryMap);
            algoliaService.saveIndex(inventoryMap);
            return tuple.getT2();
        }));
    }
    /*
     transforms the fetched consumer record into index object
     */
    private Mono<InventoryMap> transformInput(ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> receivedRecord) {
        log.debug("ReceivedRecord key: {}, value: {}  ", receivedRecord.key(), receivedRecord.value());
	KafkaLocationAtpPublishKey key = receivedRecord.key();
        KafkaLocationInventoryAtpPublishEntity kafkaLocationInventoryAtpPublishEntity = receivedRecord.value();
        String productId = key.getProductId();
        String locationId = key.getLocationId();
        String locationType = key.getLocationType();
        return Flux.fromIterable(receivedRecord.value().getEntity().getAvailabilityByProducts()).reduce(new InventoryMap(), (inventoryMap, availabilityByProducts) -> {
            inventoryMap.setObjectID(productId);
            inventoryMap.putAll(fulfillmentTypeMapperService.processRecordByFulfillmentType(locationType, kafkaLocationInventoryAtpPublishEntity.getUpdateTime(), locationId, inventoryMap, availabilityByProducts, fulfillmentTypeList));
            return inventoryMap;
        });
    }
    /*
       check mandatory attributes(productId, locationId, locationType)
     */
    private void checkForMandatoryAttributes(ReceiverRecord<KafkaLocationAtpPublishKey, KafkaLocationInventoryAtpPublishEntity> receivedRecord) {

        if (receivedRecord.key().getLocationType() == null || receivedRecord.key().getLocationType().length() == 0) {
            throw new NonRetryableIntegrationException("Mandatory Parameters missing :: Location Type is null or missing");
        }
        if (receivedRecord.key().getLocationId() == null || receivedRecord.key().getLocationId().length() == 0) {
            throw new NonRetryableIntegrationException("Mandatory Parameters missing :: Location Id is null or missing");
        }
        if (receivedRecord.key().getProductId() == null || receivedRecord.key().getProductId().length() == 0) {
            throw new NonRetryableIntegrationException("Mandatory Parameters missing :: Product Id is null or missing");
        }
    }
}