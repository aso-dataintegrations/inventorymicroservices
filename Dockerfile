FROM openjdk:8-jdk-alpine
ARG JAR_FILE=microservice/target/academy-microservice-RELEASE-2021.04.23-app.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-Djava.security.edg=file:/dev/./urandom","-jar","/app.jar"]